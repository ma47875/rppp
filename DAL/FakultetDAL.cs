﻿using System.Data.Entity;
using System.Collections.Generic;
using System.Linq;

namespace DAL
{
    public class FakultetDAL
    {
        

        public List<Fakultet> FetchFakultetAll()
        {
            using (RPPP10Entities db = new RPPP10Entities())
                return db.Fakultet.Include(x => x.Sveuciliste).AsNoTracking().ToList();
        }

        public Fakultet FetchFakultetByID(int fakID)
        {
            using (RPPP10Entities db = new RPPP10Entities())
                return db.Fakultet.Include(x => x.Sveuciliste).SingleOrDefault(x => x.Id == fakID);
        }

        public List<Fakultet> FetchFakultetsBySveuciliste(int sveucilisteID)
        {
            using (RPPP10Entities db = new RPPP10Entities())
                return db.Fakultet.Include(x => x.Sveuciliste).AsNoTracking().Where(x => x.SveucilisteId==sveucilisteID).ToList();
        }

        public void Insert(int SveucilisteId, string Naziv, string Kratica, string Adresa, string OIB, string URL, string IBAN)
        {
            using (RPPP10Entities ctx = new RPPP10Entities())
            {
                Fakultet f = new Fakultet();
                f.SveucilisteId = SveucilisteId;
                f.Naziv = Naziv;
                f.Kratica = Kratica;
                f.Adresa = Adresa;
                f.OIB = OIB;
                f.URL = URL;
                f.IBAN = IBAN;
                ctx.Fakultet.Add(f);
                ctx.SaveChanges();
            }
        }


        public void Update(int Id, int SveucilisteId, string Naziv, string Kratica, string Adresa, string OIB, string URL, string IBAN)
        {
            using (RPPP10Entities ctx = new RPPP10Entities())
            {
                Fakultet f = ctx.Fakultet.Find(Id);
                f.SveucilisteId = SveucilisteId;
                f.Naziv = Naziv;
                f.Kratica = Kratica;
                f.Adresa = Adresa;
                f.OIB = OIB;
                f.URL = URL;
                f.IBAN = IBAN;
                ctx.SaveChanges();
            }
        }


        public void Delete(int FakultetID)
        {
            using (RPPP10Entities ctx = new RPPP10Entities())
            {
                Fakultet f = ctx.Fakultet.Find(FakultetID);
                ctx.Fakultet.Remove(f);
                ctx.SaveChanges();
            }
        }
    }
}
