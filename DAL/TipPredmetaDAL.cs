﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class TipPredmetaDAL
    {
        public List<TipPredmeta> FetchAll()
        {
            using (var context = new RPPP10Entities())
            {
                return context.TipPredmeta.AsNoTracking()
                    .ToList();
            }
        }

        public TipPredmeta FetchById(int id)
        {
            using (var context = new RPPP10Entities())
            {
                return context.TipPredmeta.AsNoTracking()
                    .FirstOrDefault(x => x.Id == id);
            }
        }
        
        public void Insert(string naziv) {
            using (var context = new RPPP10Entities())
            {
                var tp = new TipPredmeta()
                {
                    Id = context.StudijskiProgram.Max(x => x.Id) + 1,
                    Naziv = naziv
                };
                context.TipPredmeta.Add(tp);
                context.SaveChanges();
            }
        }

        public void Update(int id, string naziv)
        {
            using (var context = new RPPP10Entities())
            {
                var tp = context.TipPredmeta.Find(id);

                tp.Id = id;
                tp.Naziv = naziv;

                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var context = new RPPP10Entities())
            {
                var tp = context.TipPredmeta.Find(id);
                context.TipPredmeta.Remove(tp);
                context.SaveChanges();
            }
        }
    }
}
