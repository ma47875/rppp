﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class PredmetDAL
    {
        public List<Predmet> FetchAll(int? studijskiProgramId = null)
        {
            using (RPPP10Entities context = new DAL.RPPP10Entities())
            {
                var rv = context.Predmet
                    .Include(p => p.StudijskiProgram)
                    .Include(p => p.TipPredmeta)
                    .ToList();

                if (studijskiProgramId.HasValue)
                    rv = rv
                        //.Include(p => p.StudijskiProgram).Include(p => p.TipPredmeta)
                        .Where(x => x.StudijskiProgramId == studijskiProgramId).ToList();

                return rv;
            }
        }

        public List<Predmet> FetchByStudijskiProgramId(int id)
        {
            using (RPPP10Entities context = new RPPP10Entities())
            {
                return context.Predmet
                    .Include(x => x.StudijskiProgram)
                    .Include(x => x.TipPredmeta)
                    .Where(x => x.StudijskiProgramId == id)
                    .ToList();
            }
        }

        public Predmet FetchById(int id)
        {
            using (var context = new RPPP10Entities())
            {
                return context.Predmet
                    .Include(p => p.StudijskiProgram)
                    .Include(p => p.TipPredmeta)
                    .FirstOrDefault(x => x.Id == id);
            }
        }

        public void Insert(int tipPredmetaId, int studijskiProgramId, string naziv, string ishodiUcenja)
        {
            using (var context = new RPPP10Entities())
            {
                var p = new Predmet()
                {
                    TipPredmetaId = tipPredmetaId,
                    StudijskiProgramId = studijskiProgramId,
                    Naziv = naziv,
                    IshodiUcenja = ishodiUcenja
                };
                context.Predmet.Add(p);
                context.SaveChanges();
            }
        }

        public void Update(int id, int tipPredmetaId, int studijskiProgramId, string naziv, string ishodiUcenja)
        {
            using (var context = new RPPP10Entities())
            {
                var p = context.Predmet.Find(id);

                p.TipPredmetaId = tipPredmetaId;
                p.StudijskiProgramId = studijskiProgramId;
                p.Naziv = naziv;
                p.IshodiUcenja = ishodiUcenja;

                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var context = new RPPP10Entities())
            {
                var p = context.Predmet.Find(id);
                context.Predmet.Remove(p);
                context.SaveChanges();
            }
        }
    }
}
