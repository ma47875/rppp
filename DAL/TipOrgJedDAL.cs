﻿using System.Collections.Generic;
using System.Linq;

namespace DAL
{
    public class TipOrgJedDAL
    {

        public List<TipOrgJed> FetchAll()
        {
            using (RPPP10Entities db = new RPPP10Entities())
                return db.TipOrgJed.ToList();
        }

        public TipOrgJed FetchTipOjByID(int tipOrgJedID)
        {
            using (RPPP10Entities db = new RPPP10Entities())
                return db.TipOrgJed.SingleOrDefault(a => a.Id == tipOrgJedID);
        }

        public void Insert(string TipOrgJedNaziv)
        {
            using (RPPP10Entities ctx = new RPPP10Entities())
            {
                TipOrgJed toj = new TipOrgJed();
                toj.Naziv = TipOrgJedNaziv;
                ctx.TipOrgJed.Add(toj);
                ctx.SaveChanges();
            }
        }


        public void Update(int tipOrgJedID, string tipOrgJedNaziv)
        {
            using (RPPP10Entities ctx = new RPPP10Entities())
            {
                TipOrgJed toj = ctx.TipOrgJed.Find(tipOrgJedID);
                toj.Naziv = tipOrgJedNaziv;
                ctx.SaveChanges();
            }
        }


        public void Delete(int tipOrgJedID)
        {
            using (RPPP10Entities ctx = new RPPP10Entities())
            {
                TipOrgJed toj = ctx.TipOrgJed.Find(tipOrgJedID);
                ctx.TipOrgJed.Remove(toj);
                ctx.SaveChanges();
            }
        }
    }
}
