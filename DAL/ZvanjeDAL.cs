﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class ZvanjeDAL
    {
        public List<Zvanje> FetchAll()
        {
            using (var context = new RPPP10Entities())
            {
                return context.Zvanje.AsNoTracking().ToList();
            }
        }

        public Zvanje Fetch(int id)
        {
            using (var context = new RPPP10Entities())
            {
                return context.Zvanje.Where(d => d.Id == id).SingleOrDefault();
            }
        }

        public void Insert(int Id, String Naziv)
        {
            using (var context = new RPPP10Entities())
            {
                Zvanje z = new Zvanje();
                z.Id = Id;
                z.Naziv = Naziv;
                context.Zvanje.Add(z);
                context.SaveChanges();
            }
        }

        public void Update(int Id, String Naziv)
        {
            using (var context = new RPPP10Entities())
            {
                Zvanje z = context.Zvanje.Find(Id);
                z.Id = Id;
                z.Naziv = Naziv;
                context.SaveChanges();
            }
        }

        public void Delete(int Id)
        {
            using (var context = new RPPP10Entities())
            {
                Zvanje z = context.Zvanje.Find(Id);
                context.Zvanje.Remove(z);
                context.SaveChanges();
            }
        }
    }
}
