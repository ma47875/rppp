﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class ZnanstveniRadDAL
    {
        public List<ZnanstveniRad> FetchAll()
        {
            using (var context = new RPPP10Entities())
            {
                return context.ZnanstveniRad.Include(z => z.TipZnanstvenogRada).AsNoTracking().ToList();
            }
        }

        public ZnanstveniRad Fetch(int id)
        {
            using (var context = new RPPP10Entities())
            {
                return context.ZnanstveniRad.Include(z => z.TipZnanstvenogRada).Where(d => d.Id == id).SingleOrDefault();
            }
        }

        public void Insert(int Id, int NastavnoOsobljeId, int TipZnanstvenogRadaId, string Naziv, int Godina, string SifraUpisa)
        {
            using (var context = new RPPP10Entities())
            {
                ZnanstveniRad z = new ZnanstveniRad();
                z.Id = context.ZnanstveniRad.Max(r => r.Id) + 1;
                z.NastavnoOsobljeId = NastavnoOsobljeId;
                z.TipZnanstvenogRadaId = TipZnanstvenogRadaId;
                z.Naziv = Naziv;
                z.Godina = Godina;
                z.SifraUpisa = SifraUpisa;
                context.ZnanstveniRad.Add(z);
                context.SaveChanges();
            }
        }

        public void Update(int Id, int NastavnoOsobljeId, int TipZnanstvenogRadaId, string Naziv, int Godina, string SifraUpisa)
        {
            using (var context = new RPPP10Entities())
            {
                ZnanstveniRad z = context.ZnanstveniRad.Find(Id);
                z.Id = Id;
                z.NastavnoOsobljeId = NastavnoOsobljeId;
                z.TipZnanstvenogRadaId = TipZnanstvenogRadaId;
                z.Naziv = Naziv;
                z.Godina = z.Godina;
                z.SifraUpisa = SifraUpisa;
                context.SaveChanges();
            }
        }

        public void Delete(int Id)
        {
            using (var context = new RPPP10Entities())
            {
                ZnanstveniRad z = context.ZnanstveniRad.Find(Id);
                context.ZnanstveniRad.Remove(z);
                context.SaveChanges();
            }
        }
    }
}
