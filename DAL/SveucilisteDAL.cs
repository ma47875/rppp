﻿using System.Collections.Generic;
using System.Linq;

namespace DAL
{
    public class SveucilisteDAL
    {

        public List<Sveuciliste> FetchAll()
        {
            using (RPPP10Entities db = new RPPP10Entities())
            {
                return db.Sveuciliste.AsNoTracking().ToList();
            }
        }

        public Sveuciliste FetchSveucilisteByID(int svID)
        {
            using (RPPP10Entities db = new RPPP10Entities())
                return db.Sveuciliste.SingleOrDefault(x => x.Id == svID);
        }
    }
}
