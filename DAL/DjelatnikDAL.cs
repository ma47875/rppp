﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class DjelatnikDAL
    {
        public List<Djelatnik> FetchAll()
        {
            using (var context = new RPPP10Entities())
            {
                return context.Djelatnik.Include(d => d.OrganizacijskaJedinica).Include(d => d.TipDjelatnika).Include(d => d.Zaposlenje).Include(d => d.Zvanje).ToList();
            }
        }

        public List<Djelatnik> FetchByOrgJed(int orgJedId)
        {
            using (var context = new RPPP10Entities())
            {
                return context.Djelatnik.Include(d => d.OrganizacijskaJedinica).Include(d => d.TipDjelatnika).Include(d => d.Zaposlenje).Include(d => d.Zvanje).Where(d => d.OrgJedId==orgJedId).ToList();
            }
        }

        public Djelatnik Fetch(int id)
        {
            using (var context = new RPPP10Entities())
            {
                return context.Djelatnik.Include(d => d.OrganizacijskaJedinica).Include(d => d.TipDjelatnika).Include(d => d.Zaposlenje).Include(d => d.Zvanje).Where(d => d.Id == id).SingleOrDefault();
            }
        }

        public void Insert(int Id,int ZaposlenjeId, int OrgJedId, string Ime, string Prezime, string OIB, int GodRodjenja,int ZvanjeId ,string UredSoba,string E_mail,string Telefon,int TipDjelatnikaId )
        {
            using (var context = new RPPP10Entities())
            {
                Djelatnik d = new Djelatnik();
                d.Id = Id;
                d.ZaposlenjeId = ZaposlenjeId;
                d.OrgJedId = OrgJedId;
                d.Ime = Ime;
                d.Prezime = Prezime;
                d.OIB = OIB;
                d.GodRodjenja = GodRodjenja;
                d.ZvanjeId = ZvanjeId;
                d.UredSoba = UredSoba;
                d.E_mail = E_mail;
                d.Telefon = Telefon;
                d.TipDjelatnikaId = TipDjelatnikaId;
                context.Djelatnik.Add(d);
                context.SaveChanges();
            }
        }

        public void Update(int Id, int ZaposlenjeId, int OrgJedId, string Ime, string Prezime, string OIB, int GodRodjenja, int ZvanjeId, string UredSoba, string E_mail, string Telefon, int TipDjelatnikaId)
        {
            using (var context = new RPPP10Entities())
            {
                Djelatnik d = context.Djelatnik.Find(Id) ;
                d.Id = Id;
                d.ZaposlenjeId = ZaposlenjeId;
                d.OrgJedId = OrgJedId;
                d.Ime = Ime;
                d.Prezime = Prezime;
                d.OIB = OIB;
                d.GodRodjenja = GodRodjenja;
                d.ZvanjeId = ZvanjeId;
                d.UredSoba = UredSoba;
                d.E_mail = E_mail;
                d.Telefon = Telefon;
                d.TipDjelatnikaId = TipDjelatnikaId;
                context.SaveChanges();
            }
        }

        public void Delete(int Id)
        {
            using (var context = new RPPP10Entities())
            {
                Djelatnik d = context.Djelatnik.Find(Id);
                var radovi = context.ZnanstveniRad.Where(r => r.NastavnoOsobljeId == Id).ToList();
                foreach ( ZnanstveniRad z in radovi)
                {
                    context.ZnanstveniRad.Remove(z);
                }
                context.Djelatnik.Remove(d);
                context.SaveChanges();
            }
        }
    }
}
