﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class NastavniPlanDAL
    {
        public List<NastavniPlan> Fetch(int? predmetId = null, int? semestar = null, int? godina = null)
        {
            using (var context = new RPPP10Entities())
            {
                var rv = context.NastavniPlan.AsNoTracking()
                    .Include(x => x.Predmet)
                    .ToList();

                if (predmetId.HasValue && predmetId != -1)
                {
                    rv = rv.Where(x => x.PredmetId == predmetId).ToList();
                }

                if (semestar.HasValue && semestar != -1)
                {
                    rv = rv.Where(x => x.Semestar == semestar).ToList();
                }

                return rv;
            }
        }

        public NastavniPlan FetchById(int id)
        {
            using (var context = new RPPP10Entities())
            {
                return context.NastavniPlan.AsNoTracking()
                    .Include(x => x.Predmet)
                    .FirstOrDefault(x => x.Id == id);
            }
        }

        public NastavniPlan Fetch(int id)
        {
            using (var context = new RPPP10Entities())
            {
                return context.NastavniPlan.AsNoTracking()
                    .Include(x => x.Predmet)
                    .FirstOrDefault(x => x.Id == id);
            }
        }

        public void Insert(int predmetId, int semestar, int godina, int ects, int satiPredavanja, int satiLaboratorija, int satiVjezbi) {
            using (var context = new RPPP10Entities())
            {
                var np = new NastavniPlan()
                {
                    PredmetId = predmetId,
                    Semestar = semestar, 
                    GodinaOdržavanja = godina, 
                    ECTS = ects,
                    SatiPredavanja = satiPredavanja,
                    SatiLaboratorija = satiLaboratorija,
                    SatiVježbi = satiVjezbi
                };
                context.NastavniPlan.Add(np);
                context.SaveChanges();
            }
        }

        public void Update(int id, int predmetId, int semestar, int godina, int ects, int satiPredavanja, int satiLaboratorija, int satiVjezbi)
        {
            using (var context = new RPPP10Entities())
            {
                var np = context.NastavniPlan.Find(id);
                
                np.PredmetId = predmetId;
                np.Semestar = semestar;
                np.ECTS = ects;
                np.SatiPredavanja = satiPredavanja;
                np.SatiLaboratorija = satiLaboratorija;
                np.SatiVježbi = satiVjezbi;

                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var context = new RPPP10Entities())
            {
                var np = context.NastavniPlan.Find(id);
                context.NastavniPlan.Remove(np);
                context.SaveChanges();
            }
        }
    }
}
