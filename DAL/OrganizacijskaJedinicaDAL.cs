﻿using System.Data.Entity;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DAL
{
    public class OrganizacijskaJedinicaDAL
    {

        public List<OrganizacijskaJedinica> FetchOrgJedAll()
        {
            using (RPPP10Entities db = new RPPP10Entities())
                return db.OrganizacijskaJedinica.Include(x => x.TipOrgJed).AsNoTracking().ToList();
        }

        public OrganizacijskaJedinica FetchOrgJedByID(int orgjedID)
        {
            using (RPPP10Entities db = new RPPP10Entities())
                return db.OrganizacijskaJedinica.Include(x => x.TipOrgJed).SingleOrDefault(a => a.Id == orgjedID);
        }

        public List<OrganizacijskaJedinica> FetchAllCondition(int fak)
        {
            using (RPPP10Entities db = new RPPP10Entities())
                return db.OrganizacijskaJedinica.Include(x => x.TipOrgJed).Where(m => m.FakultetId == fak).ToList();
        }

        public void Insert(int FakultetId, int TipOrgJedId, Nullable<int> KarticaOrgJedId, string Naziv, string Kratica, string Email)
        {
            using (RPPP10Entities ctx = new RPPP10Entities())
            {
                OrganizacijskaJedinica oj = new OrganizacijskaJedinica();
                oj.FakultetId = FakultetId;
                oj.TipOrgJedId = TipOrgJedId;
                oj.KarticaOrgJed = null;
                oj.Naziv = Naziv;
                oj.Kratica = Kratica;
                oj.Email = Email;
                ctx.OrganizacijskaJedinica.Add(oj);
                ctx.SaveChanges();
            }
        }


        public void Update(int orgJedId, int FakultetId, int TipOrgJedId, Nullable<int> KarticaOrgJedId, string Naziv, string Kratica, string Email)
        {
            using (RPPP10Entities ctx = new RPPP10Entities())
            {
                OrganizacijskaJedinica oj = ctx.OrganizacijskaJedinica.Find(orgJedId);
                oj.FakultetId = FakultetId;
                oj.TipOrgJedId = TipOrgJedId;
                oj.KarticaOrgJedId = KarticaOrgJedId;
                oj.Naziv = Naziv;
                oj.Kratica = Kratica;
                oj.Email = Email;
                ctx.SaveChanges();
            }
        }


        public void Delete(int orgJedId)
        {
            using (RPPP10Entities ctx = new RPPP10Entities())
            {
                OrganizacijskaJedinica oj = ctx.OrganizacijskaJedinica.Find(orgJedId);
                ctx.OrganizacijskaJedinica.Remove(oj);
                ctx.SaveChanges();
            }
        }
    }
}
