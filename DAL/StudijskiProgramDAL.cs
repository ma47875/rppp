﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class StudijskiProgramDAL
    {
        public List<StudijskiProgram> FetchAll(int? fakultetId = null, int? tipStudijaId = null)
        {
            using (var context = new RPPP10Entities())
            {
                var rv = context.StudijskiProgram.AsNoTracking()
                    .Include(x => x.TipStudija).Include(x => x.Fakultet).Include(x => x.Fakultet.Sveuciliste)
                    .ToList();

                if (fakultetId.HasValue && fakultetId != -1)
                {
                    rv = rv.Where(x => x.FakultetId == fakultetId).ToList();
                }

                if (tipStudijaId.HasValue && tipStudijaId != -1)
                {
                    rv = rv.Where(x => x.TipStudijaId == tipStudijaId).ToList();
                }

                return rv;
            }
        }

        public StudijskiProgram FetchById(int id)
        {
            using (var context = new RPPP10Entities())
            {
                return context.StudijskiProgram.AsNoTracking()
                    .Include(x => x.TipStudija).Include(x => x.Fakultet).Include(x => x.Fakultet.Sveuciliste)
                    .FirstOrDefault(x => x.Id == id);
            }
        }

        public List<StudijskiProgram> FetchByFakultetId(int id)
        {
            using (var context = new RPPP10Entities())
            {
                return context.StudijskiProgram.AsNoTracking()
                    .Include(x => x.TipStudija).Include(x => x.Fakultet).Include(x => x.Fakultet.Sveuciliste)
                    .Where(x => x.FakultetId == id).ToList();
            }
        }

        public List<Predmet> FetchPredmetiById(int id)
        {
            using (var context = new RPPP10Entities())
            {
                return context.Predmet.AsNoTracking()
                    .Where(x => x.StudijskiProgramId == id)
                    .ToList();
            }
        }

        public StudijskiProgram Fetch(int id)
        {
            using (var context = new RPPP10Entities())
            {
                return context.StudijskiProgram.AsNoTracking()
                    .Include("TipStudija")
                    .FirstOrDefault(x => x.Id == id);
            }
        }

        public void Insert(string naziv, int trajanje, int tipStudijaId, int fakultetId) {
            using (var context = new RPPP10Entities())
            {
                var sp = new StudijskiProgram()
                {
                    Id = context.StudijskiProgram.Max(x => x.Id) + 1,
                    Naziv = naziv,
                    Trajanje = trajanje,
                    TipStudijaId = tipStudijaId,
                    FakultetId = fakultetId
                };
                context.StudijskiProgram.Add(sp);
                context.SaveChanges();
            }
        }

        public void Update(int id, string naziv, int trajanje, int tipStudija, int fakultetId)
        {
            using (var context = new RPPP10Entities())
            {
                var sp = context.StudijskiProgram.Find(id);

                //sp.Id = id;
                sp.Naziv = naziv;
                sp.Trajanje = trajanje;
                sp.TipStudijaId = tipStudija;
                sp.FakultetId = fakultetId;

                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var context = new RPPP10Entities())
            {
                var sp = context.StudijskiProgram.Find(id);
                context.StudijskiProgram.Remove(sp);
                context.SaveChanges();
            }
        }
    }
}
