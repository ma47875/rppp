﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class TipZnanstvenogRadaDAL
    {
        public List<TipZnanstvenogRada> FetchAll()
        {
            using (var context = new RPPP10Entities())
            {
                return context.TipZnanstvenogRada.AsNoTracking().ToList();
            }
        }

        public TipZnanstvenogRada Fetch(int id)
        {
            using (var context = new RPPP10Entities())
            {
                return context.TipZnanstvenogRada.Where(d => d.Id == id).SingleOrDefault();
            }
        }

        public void Update(int Id, String Naziv)
        {
            using (var context = new RPPP10Entities())
            {
                TipZnanstvenogRada z = context.TipZnanstvenogRada.Find(Id);
                z.Id = Id;
                z.Naziv = Naziv;
                context.SaveChanges();
            }
        }

        public void Insert(int Id, String Naziv)
        {
            using (var context = new RPPP10Entities())
            {
                TipZnanstvenogRada z = new TipZnanstvenogRada();
                z.Id = Id;
                z.Naziv = Naziv;
                context.TipZnanstvenogRada.Add(z);
                context.SaveChanges();
            }
        }

        public void Delete(int Id)
        {
            using (var context = new RPPP10Entities())
            {
                TipZnanstvenogRada z = context.TipZnanstvenogRada.Find(Id);
                context.TipZnanstvenogRada.Remove(z);
                context.SaveChanges();
            }
        }
    }
}
