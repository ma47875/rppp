﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class TipStudijaDAL
    {
        public List<TipStudija> FetchAll()
        {
            using (var context = new RPPP10Entities())
            {
                return context.TipStudija.AsNoTracking()
                    .ToList();
            }
        }

        public TipStudija FetchById(int id)
        {
            using (var context = new RPPP10Entities())
            {
                return context.TipStudija.AsNoTracking()
                    .FirstOrDefault(x => x.Id == id);
            }
        }
        
        public void Insert(string naziv) {
            using (var context = new RPPP10Entities())
            {
                var tp = new TipStudija()
                {
                    Id = context.TipStudija.Max(x => x.Id) + 1,
                    Naziv = naziv
                };
                context.TipStudija.Add(tp);
                context.SaveChanges();
            }
        }

        public void Update(int id, string naziv)
        {
            using (var context = new RPPP10Entities())
            {
                var ts = context.TipStudija.Find(id);

                ts.Id = id;
                ts.Naziv = naziv;

                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var context = new RPPP10Entities())
            {
                var ts = context.TipStudija.Find(id);
                context.TipStudija.Remove(ts);
                context.SaveChanges();
            }
        }
    }
}
