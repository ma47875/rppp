﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAL
{
    public static class UpdateModel
    {
        public static bool TryUpdate(RPPP10Entities context)
        {
            var modelErrors = context.GetValidationErrors();
            var sb = new StringBuilder();

            foreach (var modelError in modelErrors)
            {
                if (!modelError.IsValid)
                {
                    var list = modelError.ValidationErrors
                        .Select(e => e.PropertyName + ": " + e.ErrorMessage)
                        .ToList();
                    sb.Append(string.Join("\n\n", list));
                }
            }

            if (sb.Length > 0)
            {
                MessageBox.Show(sb.ToString(), "Pogreška pri unosu", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {

                try
                {
                    context.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    CentralException.Display(ex);
                    return false;
                }
            }
            return false;
        }

        public static void CancelAllChanges(RPPP10Entities context)
        {
            foreach (var entry in context.ChangeTracker.Entries())
            {
                if (entry.State == EntityState.Added)
                {
                    entry.State = EntityState.Detached;
                }
                else if (entry.State == EntityState.Deleted || entry.State == EntityState.Modified)
                {
                    entry.State = EntityState.Unchanged;
                }
            }
        }
    }
}
