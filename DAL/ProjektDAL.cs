﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class ProjektDAL
    {
        public List<Projekt> FetchAll()
        {
            using (RPPP10Entities context = new RPPP10Entities())
            {
                return context.Projekt.ToList();
            }
        }

        public Projekt Fetch(int Id)
        {
            using (RPPP10Entities context = new RPPP10Entities())
            {
                return context.Projekt.Where(projekt => projekt.Id == Id).SingleOrDefault();
            }
        }

        public Projekt Fetch(string Naziv)
        {
            using (RPPP10Entities context = new RPPP10Entities())
            {
                return context.Projekt.Where(projekt => projekt.Naziv == Naziv).SingleOrDefault();
            }
        }

        public void Insert(int Id, string Naziv, int TipProjektaId, int KarticaProjektaId)
        {
            using (RPPP10Entities context = new RPPP10Entities())
            {
                Projekt noviProjekt = new Projekt()
                {
                    Id = Id,
                    Naziv = Naziv,
                    KarticaProjektaId = KarticaProjektaId,
                    TipProjektaId = TipProjektaId
                };
                context.Projekt.Add(noviProjekt);
                context.SaveChanges();
            }
        }

        public void Update(int Id, string Naziv, int TipProjektaId, int KarticaProjektaId)
        {
            using (RPPP10Entities context = new RPPP10Entities())
            {
                Projekt projektIzmjena = context.Projekt.Where(projekt => projekt.Id == Id).Single();
                projektIzmjena.Naziv = Naziv;
                projektIzmjena.KarticaProjektaId = KarticaProjektaId;
                projektIzmjena.TipProjektaId = TipProjektaId;
                context.SaveChanges();
            }
        }

        public void Delete(int Id)
        {
            using (RPPP10Entities context = new RPPP10Entities())
            {
                Projekt izbrisiProjekt = context.Projekt.Where(projekt => projekt.Id == Id).Single();
                context.Projekt.Remove(izbrisiProjekt);
                context.SaveChanges();
            }
        }

    }
}
