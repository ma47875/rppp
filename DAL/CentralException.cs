﻿using System;
using System.Windows.Forms;

namespace DAL
{
    public static class CentralException
    {
        public static void Display (Exception ex, string title = "Pogreška")
        {
            string msg = ex.Message + Environment.NewLine + ex.StackTrace;
            if (ex.InnerException != null)
            {
                msg += Environment.NewLine + ex.InnerException.Message;
            }
            MessageBox.Show(msg, title,
                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
