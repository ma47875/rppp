﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class ZaposlenjeDAL
    {
        public List<Zaposlenje> FetchAll()
        {
            using (var context = new RPPP10Entities())
            {
                return context.Zaposlenje.AsNoTracking().ToList();
            }
        }

        public Zaposlenje Fetch(int id)
        {
            using (var context = new RPPP10Entities())
            {
                return context.Zaposlenje.Where(d => d.Id == id).SingleOrDefault();
            }
        }

        public void Insert(int Id, String Naziv)
        {
            using (var context = new RPPP10Entities())
            {
                Zaposlenje z = new Zaposlenje();
                z.Id = Id;
                z.Naziv = Naziv;
                context.Zaposlenje.Add(z);
                context.SaveChanges();
            }
        }

        public void Update(int Id, String Naziv)
        {
            using (var context = new RPPP10Entities())
            {
                Zaposlenje z = context.Zaposlenje.Find(Id);
                z.Id = Id;
                z.Naziv = Naziv;
                context.SaveChanges();
            }
        }

        public void Delete(int Id)
        {
            using (var context = new RPPP10Entities())
            {
                Zaposlenje z = context.Zaposlenje.Find(Id);
                context.Zaposlenje.Remove(z);
                context.SaveChanges();
            }
        }
    }
}
