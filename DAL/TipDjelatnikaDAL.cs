﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class TipDjelatnikaDAL
    {
        public List<TipDjelatnika> FetchAll()
        {
            using (var context = new RPPP10Entities())
            {
                return context.TipDjelatnika.AsNoTracking().ToList();
            }
        }

        public TipDjelatnika Fetch(int id)
        {
            using (var context = new RPPP10Entities())
            {
                return context.TipDjelatnika.Where(d => d.Id == id).SingleOrDefault();
            }
        }
    }
}
