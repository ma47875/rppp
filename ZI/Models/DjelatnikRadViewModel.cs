﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL;

namespace ZI.Models
{
    public class DjelatnikRadViewModel
    {
        public Djelatnik djelatnik { get; set; }
        public List<ZnanstveniRad> radovi { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}