﻿namespace ZI.Models
{
    public class PageRangeModel
    {
        public int Min { get; set; }
        public int Max { get; set; }
        public string Url { get; set; }
    }
}