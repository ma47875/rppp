﻿using BLL;
using System.Collections.Generic;

namespace ZI.Models
{
    public class FakultetViewModel
    {
        public Fakultet Fakultet { get; set; }
        public IEnumerable<OrganizacijskaJedinica> OrgJedList { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}