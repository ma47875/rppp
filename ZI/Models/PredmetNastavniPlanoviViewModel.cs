﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL;

namespace ZI.Models
{
    public class PredmetNastavniPlanoviViewModel
    {
        public Predmet Predmet { get; set; }
        public List<NastavniPlan> NastavniPlanovi { get; set; }
    }
}