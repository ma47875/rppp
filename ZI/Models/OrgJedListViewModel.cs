﻿using BLL;
using System.Collections.Generic;

namespace ZI.Models
{
    public class OrgJedListViewModel
    {
        public IEnumerable<OrganizacijskaJedinica> OrgJedList { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}