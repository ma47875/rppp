﻿using BLL;
using System.Collections.Generic;

namespace ZI.Models
{
    public class StudijskiProgramViewModel
    {
        public StudijskiProgram StudijskiProgram { get; set; }
        public List<Predmet> Predmeti { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}