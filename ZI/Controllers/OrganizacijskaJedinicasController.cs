﻿using System.Net;
using System.Web.Mvc;

namespace DZ22.Controllers
{
    /// <summary>
    /// Upravljač za rad s organizacijskim jediniama (Detalji) koje pripadaju odabranom fakultetu (Master).
    /// </summary>
    public class OrganizacijskaJedinicasController : Controller
    {
        private BLL.BLLProviders.OrganizacijskaJedinicaBLL ojBLL = new BLL.BLLProviders.OrganizacijskaJedinicaBLL();


        /// <summary>
        /// GET postupak za prikaz Index stranice za org.jedinice
        /// Dohvaća popis svih org.jedinica
        /// </summary>
        /// <returns>Rezultat akcije upravljača prikazuje popis org.jedinica.</returns>
        public ActionResult Index()
        {
            return View(ojBLL.FetchAll());
        }


        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organizacijskaJedinica = ojBLL.FetchByID(id.Value);
            if (organizacijskaJedinica == null)
            {
                return HttpNotFound();
            }
            return View(organizacijskaJedinica);
        }

        /// <summary>
        /// Upravitelj za dohvaćanje podataka potrebnih za stranicu za kreiranje org.jedinica.
        /// </summary>
        /// <param name="fakID">Šifra fakulteta kojem pripada org.jedinica koju kreiramo</param>
        /// <returns>Pogled za kreiranje org.jedinica</returns>
        public ActionResult Create(int? fakID)
        {

            var fakBLL = new BLL.BLLProviders.FakultetBLL();
            var tojBLL = new BLL.BLLProviders.TipOrgJedBLL();
            ViewBag.FakultetId = new SelectList(fakBLL.FetchAll(), "Id", "Naziv", fakID);
            ViewBag.TipOrgJedId = new SelectList(tojBLL.FetchAll(), "Id", "Naziv");

            return View();
        }

        /// <summary>
        /// Upravitelj za dohvaćanje podataka potrebnih za stranicu za kreiranje org.jedinica.
        /// </summary>
        /// <param name="organizacijskaJedinica">Nova org.jed koja se dodaje</param>
        /// <returns>Pogled za kreiranje org.jedinica</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public  ActionResult Create([Bind(Include = "FakultetId,TipOrgJedId,Naziv,Kratica,Email")] BLL.OrganizacijskaJedinica organizacijskaJedinica)
        {
            if (ModelState.IsValid)
            {
                ojBLL.Insert(organizacijskaJedinica);
                return RedirectToAction("Details", "Fakultets", new { id = organizacijskaJedinica.FakultetId });
            }

            var fakBLL = new BLL.BLLProviders.FakultetBLL();
            object selectedFAK = fakBLL.FetchByID(organizacijskaJedinica.FakultetId);
            ViewBag.FakultetId = new SelectList(fakBLL.FetchAll(), "Id", "Naziv", selectedFAK);
            var tojBLL = new BLL.BLLProviders.TipOrgJedBLL();
            ViewBag.TipOrgJedId = new SelectList(tojBLL.FetchAll(), "Id", "Naziv", organizacijskaJedinica.TipOrgJedId);
            return View(organizacijskaJedinica);
        }


        /// <summary>
        /// Upravitelj za dohvaćanje i prikaz podataka o org.jedinici koja se uređuje.
        /// </summary>
        /// <param name="id">Broj org.jedinice koja se uređuje</param>
        /// <returns>Pogled za uređivanje org.jedinice</returns>
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organizacijskaJedinica = ojBLL.FetchByID(id.Value);
            if (organizacijskaJedinica == null)
            {
                return HttpNotFound();
            }
            var fakBLL = new BLL.BLLProviders.FakultetBLL();
            ViewBag.FakultetId = new SelectList(fakBLL.FetchAll(), "Id", "Naziv", organizacijskaJedinica.FakultetId);
            var tojBLL = new BLL.BLLProviders.TipOrgJedBLL();
            ViewBag.TipOrgJedId = new SelectList(tojBLL.FetchAll(), "Id", "Naziv", organizacijskaJedinica.TipOrgJedId);
            return View(organizacijskaJedinica);
        }

        /// <summary>
        /// POST metoda za uređivanje org.jedinica
        /// </summary>
        /// <param name="organizacijskaJedinica">Podaci o org.jed koje je potrebno spremiti</param>
        /// <returns>Stranicu Index na org.jed. koja je bila uređena</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public  ActionResult Edit([Bind(Include = "Id,FakultetId,TipOrgJedId,Naziv,Kratica,Email")] BLL.OrganizacijskaJedinica organizacijskaJedinica)
        {
            if (ModelState.IsValid)
            {
                ojBLL.Update(organizacijskaJedinica);
                return RedirectToAction("Details", "Fakultets", new { id = organizacijskaJedinica.FakultetId });
            }
            var fakBLL = new BLL.BLLProviders.FakultetBLL();
            ViewBag.FakultetId = new SelectList(fakBLL.FetchAll(), "Id", "Naziv", organizacijskaJedinica.FakultetId);
            var tojBLL = new BLL.BLLProviders.TipOrgJedBLL();
            ViewBag.TipOrgJedId = new SelectList(tojBLL.FetchAll(), "Id", "Naziv", organizacijskaJedinica.TipOrgJedId);
            return View(organizacijskaJedinica);
        }

        /// <summary>
        /// Upravitelj za dohvaćanje podataka za prikaz pogleda za potvrđivanje brisanja.
        /// </summary>
        /// <param name="id">Broj org.jed. koja se briše</param>
        /// <returns>Stranicu za potvrdu brisanja org.jed.</returns>
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var organizacijskaJedinica = ojBLL.FetchByID(id.Value);
            if (organizacijskaJedinica == null)
            {
                return HttpNotFound();
            }
            return View(organizacijskaJedinica);
        }

        /// <summary>
        /// POST metoda koja se poziva kad se potvrdi brisanje org.jed.
        /// </summary>
        /// <param name="id">Broj org.jed koja se briše</param>
        /// <returns>Osvježenu Detail stranicu s preostalim org.jed.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public  ActionResult DeleteConfirmed(int id)
        {
            var organizacijskaJedinica = ojBLL.FetchByID(id);
            ojBLL.Delete(organizacijskaJedinica);
            return RedirectToAction("Details","Fakultets", new { id = organizacijskaJedinica.FakultetId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
