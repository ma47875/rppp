﻿using BLL;
using System.Net;
using System.Web.Mvc;

namespace DZ22.Controllers
{
    public class PredmetsController : Controller
    {
        private BLL.BLLProviders.PredmetBLL bllP = new BLL.BLLProviders.PredmetBLL();
        private BLL.BLLProviders.TipPredmetaBLL bllTP = new BLL.BLLProviders.TipPredmetaBLL();
        private BLL.BLLProviders.StudijskiProgramBLL bllSP = new BLL.BLLProviders.StudijskiProgramBLL();
        private BLL.BLLProviders.NastavniPlanBLL bllNP = new BLL.BLLProviders.NastavniPlanBLL();

        // GET: Predmets
        public ActionResult Index()
        {
            var predmet = bllP.FetchAll();
            return View(predmet);
        }

        // GET: Predmets/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var predmet = bllP.FetchById(id.Value);
            if (predmet == null)
            {
                return HttpNotFound();
            }
            var model = new ZI.Models.PredmetNastavniPlanoviViewModel()
            {
                Predmet = predmet,
                NastavniPlanovi = bllNP.Fetch(predmet.Id)
            };
            return View(model);
        }

        // GET: Predmets/Create
        public ActionResult Create(int? studijskiProgramId)
        {
            object selectedSP = studijskiProgramId.HasValue ? bllSP.FetchById(studijskiProgramId.Value) : null;
            ViewBag.StudijskiProgramId = new SelectList(bllSP.FetchAll(), "Id", "Naziv", selectedSP);
            ViewBag.TipPredmetaId = new SelectList(bllTP.FetchAll(), "Id", "Naziv");
            return View();
        }

        // POST: Predmets/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(int? studijskiProgramId,
            [Bind(Include = "Id,TipPredmetaId,StudijskiProgramId,Naziv,IshodiUcenja")] Predmet predmet)
        {
            if (ModelState.IsValid)
            {
                bllP.Insert(predmet);
                return RedirectToAction("Index");
            }
            
            ViewBag.StudijskiProgramId = new SelectList(bllSP.FetchAll(), "Id", "Naziv", predmet.StudijskiProgramId);
            ViewBag.TipPredmetaId = new SelectList(bllTP.FetchAll(), "Id", "Naziv");
            return View(predmet);
        }

        // GET: Predmets/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Predmet predmet = bllP.FetchById(id.Value);
            if (predmet == null)
            {
                return HttpNotFound();
            }
            
            ViewBag.StudijskiProgramId = new SelectList(bllSP.FetchAll(), "Id", "Naziv", predmet.StudijskiProgramId);
            ViewBag.TipPredmetaId = new SelectList(bllTP.FetchAll(), "Id", "Naziv", predmet.TipPredmetaId);
            return View(predmet);
        }

        // POST: Predmets/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TipPredmetaId,StudijskiProgramId,Naziv,IshodiUcenja")] Predmet predmet)
        {
            if (ModelState.IsValid)
            {
                bllP.Update(predmet);
                return RedirectToAction("Details", new { id = predmet.Id });
            }
            
            ViewBag.StudijskiProgramId = new SelectList(bllSP.FetchAll(), "Id", "Naziv", predmet.StudijskiProgramId);
            ViewBag.TipPredmetaId = new SelectList(bllTP.FetchAll(), "Id", "Naziv", predmet.TipPredmetaId);
            return View(predmet);
        }

        // GET: Predmets/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Predmet predmet = bllP.FetchById(id.Value);
            if (predmet == null)
            {
                return HttpNotFound();
            }
            return View(predmet);
        }

        // POST: Predmets/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            bllP.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
