﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BLL;

namespace DZ22.Controllers
{
    public class ZaposlenjesController : Controller
    {
        private BLL.BLLProviders.ZaposlenjeBLL zap_p = new BLL.BLLProviders.ZaposlenjeBLL();

        // GET: Zaposlenjes
        public ActionResult Index()
        {
            return View(zap_p.FetchAll());
        }

        // GET: Zaposlenjes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zaposlenje zaposlenje = zap_p.Fetch(id.Value);
            if (zaposlenje == null)
            {
                return HttpNotFound();
            }
            return View(zaposlenje);
        }

        // GET: Zaposlenjes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Zaposlenjes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Naziv")] Zaposlenje zaposlenje)
        {
            if (ModelState.IsValid)
            {
                zap_p.Insert(zaposlenje);
                return RedirectToAction("Index");
            }

            return View(zaposlenje);
        }

        // GET: Zaposlenjes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zaposlenje zaposlenje = zap_p.Fetch(id.Value);
            if (zaposlenje == null)
            {
                return HttpNotFound();
            }
            return View(zaposlenje);
        }

        // POST: Zaposlenjes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Naziv")] Zaposlenje zaposlenje)
        {
            if (ModelState.IsValid)
            {
                zap_p.Update(zaposlenje);
                return RedirectToAction("Index");
            }
            return View(zaposlenje);
        }

        // GET: Zaposlenjes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zaposlenje zaposlenje = zap_p.Fetch(id.Value);
            if (zaposlenje == null)
            {
                return HttpNotFound();
            }
            return View(zaposlenje);
        }

        // POST: Zaposlenjes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Zaposlenje zaposlenje = zap_p.Fetch(id);
            zap_p.Delete(zaposlenje);
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
