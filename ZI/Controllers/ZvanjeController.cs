﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BLL;

namespace DZ22.Controllers
{
    public class ZvanjeController : Controller
    {
        private BLL.BLLProviders.ZvanjeBLL zva_p = new BLL.BLLProviders.ZvanjeBLL();

        // GET: Zvanje
        public ActionResult Index()
        {
            return View(zva_p.FetchAll());
        }

        // GET: Zvanje/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zvanje zvanje = zva_p.Fetch(id.Value);
            if (zvanje == null)
            {
                return HttpNotFound();
            }
            return View(zvanje);
        }

        // GET: Zvanje/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Zvanje/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Naziv")] Zvanje zvanje)
        {
            if (ModelState.IsValid)
            {

                zva_p.Insert(zvanje);
                return RedirectToAction("Index");
            }

            return View(zvanje);
        }

        // GET: Zvanje/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zvanje zvanje = zva_p.Fetch(id.Value);
            if (zvanje == null)
            {
                return HttpNotFound();
            }
            return View(zvanje);
        }

        // POST: Zvanje/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Naziv")] Zvanje zvanje)
        {
            if (ModelState.IsValid)
            {
                zva_p.Update(zvanje);
                return RedirectToAction("Index");
            }
            return View(zvanje);
        }

        // GET: Zvanje/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Zvanje zvanje = zva_p.Fetch(id.Value);
            if (zvanje == null)
            {
                return HttpNotFound();
            }
            return View(zvanje);
        }

        // POST: Zvanje/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Zvanje zvanje = zva_p.Fetch(id);
            zva_p.Delete(zvanje);
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
