﻿using System.Web.Mvc;

namespace DZ22.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}