﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DZ22.Controllers
{
    /// <summary>
    /// Upravljač za rad sa stranicama vezane uz studijske programe
    /// </summary>
    public class StudijskiProgramsController : Controller
    {
        BLL.BLLProviders.StudijskiProgramBLL bllSP = new BLL.BLLProviders.StudijskiProgramBLL();
        BLL.BLLProviders.FakultetBLL bllFak = new BLL.BLLProviders.FakultetBLL();
        BLL.BLLProviders.TipStudijaBLL bllTS = new BLL.BLLProviders.TipStudijaBLL();

        /// <summary>
        /// GET postupak za dohvaćanje podataka o studijskim programima.
        /// Omogućava filtriranje prikaza studijskih programa po fakultetu na kojem se izvode ili tipu studijskog programa
        /// </summary>
        /// <param name="fakultetId">Kriterij filtriranja po primarnom ključu fakulteta</param>
        /// <param name="tipStudijaId">Kriterij filtriranja po šifri tipa studija</param>
        /// <returns>Pogled s popisom studijskih programa (Index)</returns>
        // GET: StudijskiPrograms
        public ActionResult Index(int? fakultetId, int? tipStudijaId)
        {
            var studijskiProgrami = bllSP.FetchAll(fakultetId, tipStudijaId);

            if (fakultetId == -1) fakultetId = null;
            if (tipStudijaId == -1) tipStudijaId = null;

            var fakulteti = bllFak.FetchAll();
            fakulteti.Insert(0, new BLL.Fakultet() { Id = -1, Naziv = "--- Odaberite ---" });
            var tipoviStudija = bllTS.FetchAll();
            tipoviStudija.Insert(0, new BLL.TipStudija() { Id = -1, Naziv = "--- Odaberite ---" });
            object selectedFak = fakultetId.HasValue ? bllFak.FetchByID(fakultetId.Value) : null;
            object selectedTS = tipStudijaId.HasValue ? bllTS.Fetch(tipStudijaId.Value) : null;
            ViewData["fakultetId"] = new SelectList(fakulteti, "Id", "Naziv", selectedFak);
            ViewData["tipStudijaId"] = new SelectList(tipoviStudija, "Id", "Naziv", selectedTS);

            return View(studijskiProgrami);
        }

        /// <summary>
        /// GET postupak za dohvaćanje podataka Master-details stranice studijskog programa.
        /// Master podatak je studijski program, a details predstavljaju predmeti koji se izvode na tom studijskom programu.
        /// </summary>
        /// <param name="id">Primarni ključ studijskog programa</param>
        /// <returns>Pogled s master-details prikazom studijskog programa i pripadnih mu predmeta</returns>
        // GET: StudijskiPrograms/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var studijskiProgram = bllSP.FetchById(id.Value);
            if (studijskiProgram == null)
            {
                return HttpNotFound();
            }
            var predmeti = bllSP.FetchPredmetiById(id.Value);
            var model = new ZI.Models.StudijskiProgramViewModel()
            {
                StudijskiProgram = studijskiProgram,
                Predmeti = predmeti
            };

            return View(model);
        }

        /// <summary>
        /// GET postupak za otvaranje obrasca za unos novog studijskog programa
        /// </summary>
        /// <returns>Pogled s obrascem za unos i pohranu novog studijskog programa</returns>
        // GET: StudijskiPrograms/Create
        public ActionResult Create()
        {
            ViewBag.FakultetId = new SelectList(bllFak.FetchAll(), "Id", "Naziv");
            ViewBag.TipStudijaId = new SelectList(bllTS.FetchAll(), "Id", "Naziv");
            return View();
        }

        /// <summary>
        /// POST postupak za pohranu novog studijskog programa
        /// </summary>
        /// <param name="studijskiProgram">BLL razred koji učahuruje podatke o novounesenom studijskom programu</param>
        /// <returns>Vraća Index pogled s popisom svih studijskih programa u slučaju uspješne pohrane, a inače prikazuje prethodni obrazac za unos s pripadnim greškama</returns>
        // POST: StudijskiPrograms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "TipStudijaId,FakultetId,Naziv,Trajanje")] BLL.StudijskiProgram studijskiProgram)
        {
            if (ModelState.IsValid)
            {
                bllSP.Insert(studijskiProgram);
                return RedirectToAction("Index");
            }

            ViewBag.FakultetId = new SelectList(bllFak.FetchAll(), "Id", "Naziv", studijskiProgram.FakultetId);
            ViewBag.TipStudijaId = new SelectList(bllTS.FetchAll(), "Id", "Naziv", studijskiProgram.TipStudijaId);
            return View(studijskiProgram);
        }

        /// <summary>
        /// GET postupak za dohvaćanje podataka obrasca za uređivanje podataka o studijskom programu
        /// </summary>
        /// <param name="id">Primarni ključ studijskog programa koji se uređuje</param>
        /// <returns>Pogled s obrascem za uređivanje podataka o studijskom programu</returns>
        // GET: StudijskiPrograms/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var studijskiProgram = bllSP.FetchById(id.Value);
            if (studijskiProgram == null)
            {
                return HttpNotFound();
            }

            ViewBag.FakultetId = new SelectList(bllFak.FetchAll(), "Id", "Naziv", studijskiProgram.FakultetId);
            ViewBag.TipStudijaId = new SelectList(bllTS.FetchAll(), "Id", "Naziv", studijskiProgram.TipStudijaId);
            return View(studijskiProgram);
        }

        /// <summary>
        /// POST postupak za pohranu izmijenjenih podataka o studijskom programu
        /// </summary>
        /// <param name="studijskiProgram">BLL razred koji učahuruje izmijenjene podatke o studijskom programu</param>
        /// <returns></returns>
        // POST: StudijskiPrograms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,TipStudijaId,FakultetId,Naziv,Trajanje")] BLL.StudijskiProgram studijskiProgram)
        {
            if (ModelState.IsValid)
            {
                bllSP.Update(studijskiProgram);
                return RedirectToAction("Details", new { id = studijskiProgram.Id });
            }
            ViewBag.FakultetId = new SelectList(bllFak.FetchAll(), "Id", "Naziv", studijskiProgram.FakultetId);
            ViewBag.TipStudijaId = new SelectList(bllTS.FetchAll(), "Id", "Naziv", studijskiProgram.TipStudijaId);
            return View(studijskiProgram);
        }

        /// <summary>
        /// GET postupak za dohvaćanje podataka o studijskom programu koji se želi brisati
        /// </summary>
        /// <param name="id">Primarni ključ studijskog programa koji se želi brisati</param>
        /// <returns>Master podaci o studijskom programu koji se želi brisati</returns>
        // GET: StudijskiPrograms/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var studijskiProgram = bllSP.FetchById(id.Value);
            if (studijskiProgram == null)
            {
                return HttpNotFound();
            }
            return View(studijskiProgram);
        }

        /// <summary>
        /// POST postupak provođenje brisanja studijskog programa
        /// </summary>
        /// <param name="id">Primarni ključ studijskog programa za koji se potvrđuje brisanje</param>
        /// <returns>Pogled s popisom studijskih programa (Index) u slučaju uspjeha, a inača poruka o pogrešci.</returns>
        // POST: StudijskiPrograms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            bllSP.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
