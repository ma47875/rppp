﻿using BLL;
using System.Data.Entity;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace DZ22.Controllers
{
    public class TipPredmetasController : Controller
    {
        private BLL.BLLProviders.TipPredmetaBLL bllTP = new BLL.BLLProviders.TipPredmetaBLL();

        // GET: TipPredmetas
        public async Task<ActionResult> Index()
        {
            return View(bllTP.FetchAll());
        }

        // GET: TipPredmetas/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipPredmeta tipPredmeta = bllTP.Fetch(id.Value);
            if (tipPredmeta == null)
            {
                return HttpNotFound();
            }
            return View(tipPredmeta);
        }

        // GET: TipPredmetas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipPredmetas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Naziv")] TipPredmeta tipPredmeta)
        {
            if (ModelState.IsValid)
            {
                bllTP.Insert(tipPredmeta);
                return RedirectToAction("Index");
            }

            return View(tipPredmeta);
        }

        // GET: TipPredmetas/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipPredmeta tipPredmeta = bllTP.Fetch(id.Value);
            if (tipPredmeta == null)
            {
                return HttpNotFound();
            }
            return View(tipPredmeta);
        }

        // POST: TipPredmetas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Naziv")] TipPredmeta tipPredmeta)
        {
            if (ModelState.IsValid)
            {
                bllTP.Update(tipPredmeta);
                return RedirectToAction("Index");
            }
            return View(tipPredmeta);
        }

        // GET: TipPredmetas/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipPredmeta tipPredmeta = bllTP.Fetch(id.Value);
            if (tipPredmeta == null)
            {
                return HttpNotFound();
            }
            return View(tipPredmeta);
        }

        // POST: TipPredmetas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            bllTP.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
