﻿using BLL;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ZI.Models;

namespace DZ22.Controllers
{
    public class DjelatniksController : Controller
    {
        private BLL.BLLProviders.DjelatnikBLL p = new BLL.BLLProviders.DjelatnikBLL();
        private BLL.BLLProviders.OrganizacijskaJedinicaBLL org_p = new BLL.BLLProviders.OrganizacijskaJedinicaBLL();
        private BLL.BLLProviders.ZaposlenjeBLL zap_p = new BLL.BLLProviders.ZaposlenjeBLL();
        private BLL.BLLProviders.ZvanjeBLL zva_p = new BLL.BLLProviders.ZvanjeBLL();
        private BLL.BLLProviders.TipDjelatnikaBLL tip_p = new BLL.BLLProviders.TipDjelatnikaBLL();
        private BLL.BLLProviders.ZnanstveniRadBLL rad_p = new BLL.BLLProviders.ZnanstveniRadBLL();
        private BLL.BLLProviders.OrganizacijskaJedinicaBLL orgJed_p = new BLL.BLLProviders.OrganizacijskaJedinicaBLL();

        // GET: Djelatniks
        public ActionResult Index(int? orgJedID)
        {
            List<Djelatnik> djelatnici = new List<Djelatnik>(); 
            if (!orgJedID.HasValue || orgJedID == -1)
            {
                djelatnici = p.FetchAll();
            }
            else
            {
                djelatnici = p.FetchByOrgJed(orgJedID.Value);

            }
            var orgJed = orgJed_p.FetchAll();
            orgJed.Insert(0, new BLL.OrganizacijskaJedinica() { Id = -1, Naziv = "--- Odaberite organizacijsku jedinicu ---" });

            object orgJedSel = orgJedID.HasValue ? orgJed_p.FetchByID(orgJedID.Value) : null;
            ViewData["orgJedId"] = new SelectList(orgJed, "Id", "Naziv", orgJedSel);
            return View( djelatnici );
        }

        // GET: Djelatniks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BLL.Djelatnik djelatnik = p.Fetch(id.Value);
            if (djelatnik == null)
            {
                return HttpNotFound();
            }

            var radovi = rad_p.FetchAll().Where(x => x.NastavnoOsobljeId == djelatnik.Id);

            DjelatnikRadViewModel model = new DjelatnikRadViewModel
            {
                djelatnik = djelatnik,
                radovi = radovi.ToList(),
                PagingInfo = null
            };

            return View(model);
        }

        // GET: Djelatniks/Create
        public ActionResult Create()
        {
            ViewBag.OrgJedId = new SelectList(org_p.FetchAll(), "Id", "Naziv");
            ViewBag.TipDjelatnikaId = new SelectList(tip_p.FetchAll(), "Id", "Vrsta");
            ViewBag.ZaposlenjeId = new SelectList(zap_p.FetchAll(), "Id", "Naziv");
            ViewBag.ZvanjeId = new SelectList(zva_p.FetchAll(), "Id", "Naziv");
            return View();
        }

        // POST: Djelatniks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,ZaposlenjeId,OrgJedId,Ime,Prezime,OIB,GodRodjenja,ZvanjeId,UredSoba,E_mail,Telefon,TipDjelatnikaId")] BLL.Djelatnik djelatnik)
        {
            if (ModelState.IsValid)
            {
                //djelatnik.Id = db.Djelatnik.Max(d => d.Id) + 1;
                //djelatnik.OrgJedId = db.OrganizacijskaJedinica.FirstOrDefault(x => x.Id == djelatnik.OrgJedId).Id;
                p.Insert(djelatnik);
                return RedirectToAction("Index");
            }

            ViewBag.OrgJedId = new SelectList(org_p.FetchAll(), "Id", "Naziv", djelatnik.OrgJedId);
            ViewBag.TipDjelatnikaId = new SelectList(tip_p.FetchAll(), "Id", "Vrsta", djelatnik.TipDjelatnikaId);
            ViewBag.ZaposlenjeId = new SelectList(zap_p.FetchAll(), "Id", "Naziv", djelatnik.ZaposlenjeId);
            ViewBag.ZvanjeId = new SelectList(zva_p.FetchAll(), "Id", "Naziv", djelatnik.ZvanjeId);
            return View(djelatnik);
        }

        // GET: Djelatniks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BLL.Djelatnik djelatnik = p.Fetch(id.Value);
            if (djelatnik == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrgJedId = new SelectList(org_p.FetchAll(), "Id", "Naziv", djelatnik.OrgJedId);
            ViewBag.TipDjelatnikaId = new SelectList(tip_p.FetchAll(), "Id", "Vrsta", djelatnik.TipDjelatnikaId);
            ViewBag.ZaposlenjeId = new SelectList(zap_p.FetchAll(), "Id", "Naziv", djelatnik.ZaposlenjeId);
            ViewBag.ZvanjeId = new SelectList(zva_p.FetchAll(), "Id", "Naziv", djelatnik.ZvanjeId);
            return View(djelatnik);
        }

        // POST: Djelatniks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,ZaposlenjeId,OrgJedId,Ime,Prezime,OIB,GodRodjenja,ZvanjeId,UredSoba,E_mail,Telefon,TipDjelatnikaId")] BLL.Djelatnik djelatnik)
        {
            if (ModelState.IsValid)
            {
                p.Update(djelatnik);
                return RedirectToAction("Index");
            }
            ViewBag.OrgJedId = new SelectList(org_p.FetchAll(), "Id", "Naziv", djelatnik.OrgJedId);
            ViewBag.TipDjelatnikaId = new SelectList(tip_p.FetchAll(), "Id", "Vrsta", djelatnik.TipDjelatnikaId);
            ViewBag.ZaposlenjeId = new SelectList(zap_p.FetchAll(), "Id", "Naziv", djelatnik.ZaposlenjeId);
            ViewBag.ZvanjeId = new SelectList(zva_p.FetchAll(), "Id", "Naziv", djelatnik.ZvanjeId);
            return View(djelatnik);
        }

        // GET: Djelatniks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BLL.Djelatnik djelatnik = p.Fetch(id.Value);
            if (djelatnik == null)
            {
                return HttpNotFound();
            }
            return View(djelatnik);
        }

        // POST: Djelatniks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BLL.Djelatnik djelatnik = p.Fetch(id);
            p.Delete(djelatnik);
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
