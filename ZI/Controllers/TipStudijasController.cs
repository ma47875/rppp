﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BLL;

namespace DZ22.Controllers
{
    public class TipStudijasController : Controller
    {
        private BLL.BLLProviders.TipStudijaBLL bllTS = new BLL.BLLProviders.TipStudijaBLL();

        // GET: TipStudijas
        public ActionResult Index()
        {
            return View(bllTS.FetchAll());
        }

        // GET: TipStudijas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tipStudija = bllTS.Fetch(id.Value);
            if (tipStudija == null)
            {
                return HttpNotFound();
            }
            return View(tipStudija);
        }

        // GET: TipStudijas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipStudijas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Naziv")] BLL.TipStudija tipStudija)
        {
            if (ModelState.IsValid)
            {
                bllTS.Insert(tipStudija);
                return RedirectToAction("Index");
            }

            return View(tipStudija);
        }

        // GET: TipStudijas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tipStudija = bllTS.Fetch(id.Value);
            if (tipStudija == null)
            {
                return HttpNotFound();
            }
            return View(tipStudija);
        }

        // POST: TipStudijas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Naziv")] BLL.TipStudija tipStudija)
        {
            if (ModelState.IsValid)
            {
                bllTS.Update(tipStudija);
                return RedirectToAction("Index");
            }
            return View(tipStudija);
        }

        // GET: TipStudijas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tipStudija = bllTS.Fetch(id.Value);
            if (tipStudija == null)
            {
                return HttpNotFound();
            }
            return View(tipStudija);
        }

        // POST: TipStudijas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            bllTS.Delete(id);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
