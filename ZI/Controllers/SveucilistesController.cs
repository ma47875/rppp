﻿using System.Data.Entity;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

// Nije potrebno
/*

namespace DZ22.Controllers
{
    public class SveucilistesController : Controller
    {
        private RPPP10Entities db = new RPPP10Entities();

        // GET: Sveucilistes
        public async Task<ActionResult> Index()
        {
            return View(await db.Sveuciliste.ToListAsync());
        }

        // GET: Sveucilistes/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sveuciliste sveuciliste = await db.Sveuciliste.FindAsync(id);
            if (sveuciliste == null)
            {
                return HttpNotFound();
            }
            return View(sveuciliste);
        }

        // GET: Sveucilistes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Sveucilistes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Naziv,Kratica,Adresa,IBAN,OIB,URL")] Sveuciliste sveuciliste)
        {
            if (ModelState.IsValid)
            {
                db.Sveuciliste.Add(sveuciliste);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(sveuciliste);
        }

        // GET: Sveucilistes/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sveuciliste sveuciliste = await db.Sveuciliste.FindAsync(id);
            if (sveuciliste == null)
            {
                return HttpNotFound();
            }
            return View(sveuciliste);
        }

        // POST: Sveucilistes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Naziv,Kratica,Adresa,IBAN,OIB,URL")] Sveuciliste sveuciliste)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sveuciliste).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(sveuciliste);
        }

        // GET: Sveucilistes/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Sveuciliste sveuciliste = await db.Sveuciliste.FindAsync(id);
            if (sveuciliste == null)
            {
                return HttpNotFound();
            }
            return View(sveuciliste);
        }

        // POST: Sveucilistes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Sveuciliste sveuciliste = await db.Sveuciliste.FindAsync(id);
            db.Sveuciliste.Remove(sveuciliste);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

*/