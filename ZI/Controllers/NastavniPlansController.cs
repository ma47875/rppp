﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DZ22.Controllers
{
    /// <summary>
    /// Upravljač za rad sa stranicama vezane uz nastavne planove
    /// </summary>
    public class NastavniPlansController : Controller
    {
        BLL.BLLProviders.NastavniPlanBLL bllNP = new BLL.BLLProviders.NastavniPlanBLL();
        BLL.BLLProviders.PredmetBLL bllP = new BLL.BLLProviders.PredmetBLL();

        /// <summary>
        /// GET postupak za dohvaćanje podataka Master-details stranice predmeta i nastavnih planova.
        /// </summary>
        /// <param name="id">Primarni ključ predmeta</param>
        /// <returns>Pogled s master-details prikazom predmeta i pripadnih nastavnih planova</returns>
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var predmet = bllP.FetchById(id.Value);
            if (predmet == null)
            {
                return HttpNotFound();
            }
            var nastavniPlanovi = bllNP.Fetch(id);
            var model = new ZI.Models.PredmetNastavniPlanoviViewModel()
            {
                Predmet = predmet,
                NastavniPlanovi = nastavniPlanovi
            };

            return View(model);
        }

        /// <summary>
        /// GET postupak za otvaranje obrasca za unos novog nastavnog plana
        /// </summary>
        /// <returns>Pogled s obrascem za unos i pohranu novog nastavnog plana</returns>
        public ActionResult Create(int predmetId)
        {
            ViewBag.PredmetId = predmetId;
            return View();
        }

        /// <summary>
        /// POST postupak za pohranu novog nastavnog plana
        /// </summary>
        /// <param name="nastavniPlan">BLL razred koji učahuruje podatke o novounesenom studijskom programu</param>
        /// <returns>Vraća Index pogled s popisom svih studijskih programa u slučaju uspješne pohrane, a inače prikazuje prethodni obrazac za unos s pripadnim greškama</returns>
        // POST: StudijskiPrograms/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "PredmetId,GodinaOdrzavanja,Semestar,ECTS,SatiPredavanja,SatiLaboratorija,SatiVjezbi")] BLL.NastavniPlan nastavniPlan)
        {
            if (ModelState.IsValid)
            {
                bllNP.Insert(nastavniPlan);
                return RedirectToAction("Details", "Predmets", new { id = nastavniPlan.PredmetId });
            }
            
            return View(nastavniPlan);
        }

        /// <summary>
        /// GET postupak za dohvaćanje podataka obrasca za uređivanje podataka o nastavnom planu
        /// </summary>
        /// <param name="id">Primarni ključ nastavnog plana koji se uređuje</param>
        /// <returns>Pogled s obrascem za uređivanje podataka o nastavnom planu</returns>
        // GET: StudijskiPrograms/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var nastavniPlan = bllNP.FetchById(id.Value);
            if (nastavniPlan == null)
            {
                return HttpNotFound();
            }

            ViewBag.PredmetId = nastavniPlan.PredmetId;
            return View(nastavniPlan);
        }

        /// <summary>
        /// POST postupak za pohranu izmijenjenih podataka o nastavnom planu
        /// </summary>
        /// <param name="nastavniPlan">BLL razred koji učahuruje izmijenjene podatke o nastavnom planu</param>
        /// <returns></returns>
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, [Bind(Include = "PredmetId,GodinaOdrzavanja,Semestar,ECTS,SatiPredavanja,SatiLaboratorija,SatiVjezbi")] BLL.NastavniPlan nastavniPlan)
        {
            nastavniPlan.Id = id;
            if (ModelState.IsValid)
            {
                bllNP.Update(nastavniPlan);
                return RedirectToAction("Details", "Predmets", new { id = nastavniPlan.PredmetId });
            }

            ViewBag.PredmetId = nastavniPlan.PredmetId;
            return View(nastavniPlan);
        }

        /// <summary>
        /// GET postupak za dohvaćanje podataka o nastavnom planu koji se želi brisati
        /// </summary>
        /// <param name="id">Primarni ključ nastavnog plana koji se želi brisati</param>
        /// <returns>Master podaci o nastavnom planu koji se želi brisati</returns>
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var nastavniPlan = bllNP.FetchById(id.Value);
            if (nastavniPlan == null)
            {
                return HttpNotFound();
            }
            return View(nastavniPlan);
        }

        /// <summary>
        /// POST postupak provođenje brisanja nastavnog plana
        /// </summary>
        /// <param name="id">Primarni ključ nastavnog plana za koji se potvrđuje brisanje</param>
        /// <returns>Pogled s predmetom i popisom nastavnih planova u slučaju uspjeha, a inača poruka o pogrešci.</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var predmetId = bllNP.FetchById(id).PredmetId;
            bllNP.Delete(id);
            return RedirectToAction("Details", "Predmets", new { id = predmetId });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
