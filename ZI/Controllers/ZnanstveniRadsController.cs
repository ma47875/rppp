﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BLL;

namespace ZI.Controllers
{
    public class ZnanstveniRadsController : Controller
    {
        private BLL.BLLProviders.ZnanstveniRadBLL rad_p = new BLL.BLLProviders.ZnanstveniRadBLL();
        private BLL.BLLProviders.TipZnanstvenogRadaBLL tip_p = new BLL.BLLProviders.TipZnanstvenogRadaBLL();

        // GET: ZnanstveniRads
        public ActionResult Index()
        {
            return View(rad_p.FetchAll());
        }

        // GET: ZnanstveniRads/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ZnanstveniRad znanstveniRad = rad_p.Fetch(id.Value);
            if (znanstveniRad == null)
            {
                return HttpNotFound();
            }
            return View(znanstveniRad);
        }

        // GET: ZnanstveniRads/Create
        public ActionResult Create()
        {
            //ViewBag.NastavnoOsobljeId = new SelectList(db.Djelatnik, "Id", "Ime");
            ViewBag.TipZnanstvenogRadaId = new SelectList(tip_p.FetchAll(), "Id", "Naziv");
            return View();
        }

        // POST: ZnanstveniRads/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,NastavnoOsobljeId,TipZnanstvenogRadaId,Naziv,Godina,SifraUpisa")] ZnanstveniRad znanstveniRad)
        {
            if (ModelState.IsValid)
            {
                rad_p.Insert(znanstveniRad);
                return RedirectToAction("Details", "Djelatniks", new { id = znanstveniRad.NastavnoOsobljeId });
            }

            //ViewBag.NastavnoOsobljeId = new SelectList(db.Djelatnik, "Id", "Ime", znanstveniRad.NastavnoOsobljeId);
            ViewBag.TipZnanstvenogRadaId = new SelectList(tip_p.FetchAll(), "Id", "Naziv", znanstveniRad.TipZnanstvenogRadaId);
            return View(znanstveniRad);
        }

        // GET: ZnanstveniRads/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ZnanstveniRad znanstveniRad = rad_p.Fetch(id.Value);
            if (znanstveniRad == null)
            {
                return HttpNotFound();
            }
          //  ViewBag.NastavnoOsobljeId = new SelectList(db.Djelatnik, "Id", "Ime", znanstveniRad.NastavnoOsobljeId);
            ViewBag.TipZnanstvenogRadaId = new SelectList(tip_p.FetchAll(), "Id", "Naziv", znanstveniRad.TipZnanstvenogRadaId);
            return View(znanstveniRad);
        }

        // POST: ZnanstveniRads/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,NastavnoOsobljeId,TipZnanstvenogRadaId,Naziv,Godina,SifraUpisa")] ZnanstveniRad znanstveniRad)
        {
            if (ModelState.IsValid)
            {
                rad_p.Update(znanstveniRad);
                return RedirectToAction("Details", "Djelatniks", new { id = znanstveniRad.NastavnoOsobljeId });
            }
            // ViewBag.NastavnoOsobljeId = new SelectList(db.Djelatnik, "Id", "Ime", znanstveniRad.NastavnoOsobljeId);
            ViewBag.TipZnanstvenogRadaId = new SelectList(tip_p.FetchAll(), "Id", "Naziv", znanstveniRad.TipZnanstvenogRadaId);
            return View(znanstveniRad);
        }

        // GET: ZnanstveniRads/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ZnanstveniRad znanstveniRad = rad_p.Fetch(id.Value);
            if (znanstveniRad == null)
            {
                return HttpNotFound();
            }
            return View(znanstveniRad);
        }

        // POST: ZnanstveniRads/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ZnanstveniRad znanstveniRad = rad_p.Fetch(id);
            rad_p.Delete(znanstveniRad);
            return RedirectToAction("Details", "Djelatniks", new { id = znanstveniRad.NastavnoOsobljeId });
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
