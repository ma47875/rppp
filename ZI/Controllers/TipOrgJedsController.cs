﻿using System.Net;
using System.Web.Mvc;

namespace DZ22.Controllers
{
    public class TipOrgJedsController : Controller
    {
        private BLL.BLLProviders.TipOrgJedBLL tojBLL = new BLL.BLLProviders.TipOrgJedBLL();


        public ActionResult Index()
        {
            return View(tojBLL.FetchAll());
        }


        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tipOrgJed = tojBLL.FetchByID(id.Value);
            if (tipOrgJed == null)
            {
                return HttpNotFound();
            }
            return View(tipOrgJed);
        }


        public ActionResult Create()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Naziv")] BLL.TipOrgJed tipOrgJed)
        {
            if (ModelState.IsValid)
            {
                tojBLL.Insert(tipOrgJed);
                return RedirectToAction("Index");
            }

            return View(tipOrgJed);
        }


        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tipOrgJed = tojBLL.FetchByID(id.Value);
            if (tipOrgJed == null)
            {
                return HttpNotFound();
            }
            return View(tipOrgJed);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Naziv")] BLL.TipOrgJed tipOrgJed)
        {
            if (ModelState.IsValid)
            {
                tojBLL.Update(tipOrgJed);
                return RedirectToAction("Index");
            }
            return View(tipOrgJed);
        }


        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var tipOrgJed = tojBLL.FetchByID(id.Value);
            if (tipOrgJed == null)
            {
                return HttpNotFound();
            }
            return View(tipOrgJed);
        }


        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var tipOrgJed = tojBLL.FetchByID(id);
            tojBLL.Delete(tipOrgJed);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
