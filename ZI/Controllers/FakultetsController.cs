﻿using BLL;
using BLL.BLLProviders;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ZI.Models;

namespace DZ22.Controllers
{
    /// <summary>
    /// Upravljač za rad s master-detail stranicom Fakulteti.
    /// </summary>
    public class FakultetsController : Controller
    {
        
        private BLL.BLLProviders.FakultetBLL fakBLL = new BLL.BLLProviders.FakultetBLL();

        private BLL.BLLProviders.SveucilisteBLL sveBLL = new BLL.BLLProviders.SveucilisteBLL();
        private List<Fakultet> fakulteti = new List<Fakultet>();


        /// <summary>
        /// GET postupak za prikaz Index stranice za fakultete
        /// Dohvaća popis svih fakulteta, sortira ih po parametrima i predaje pogledu.
        /// </summary>
        /// <param name="sveucilisteId">Uvjet sortiranja fakulteta</param>
        /// <returns>Rezultat akcije upravljača prikazuje popis faulteta.</returns>
        public ActionResult Index(int? sveucilisteId)
        {
            if (!sveucilisteId.HasValue || sveucilisteId == -1) {
                fakulteti = fakBLL.FetchAll().ToList();
            } else {
                fakulteti = fakBLL.FetchBySveuciliste(sveucilisteId.Value).ToList();
                
            }
            var sveucilista = sveBLL.FetchAll();
            sveucilista.Insert(0, new BLL.Sveuciliste() { Id = -1, Naziv = "--- Odaberite sveučilište ---" });

            object svSel = sveucilisteId.HasValue ? sveBLL.FetchByID(sveucilisteId.Value) : null;
            ViewData["SveucilisteId"] = new SelectList(sveucilista, "Id", "Naziv", svSel);
            return View(fakulteti);
        }

        /// <summary>
        /// Dohvaća popis svih organizacijskih jedinica određenog fakulteta
        /// Namještava straničenje
        /// </summary>
        /// <param name="id">Šifra fakulteta čije podatke želimo prikazati</param>
        /// <returns>Rezultat akcije upravljača prikazuje pogled Details s odabranim fakultetom i pripadajućim organizacijskim jedinicama.</returns>
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var fakultet = fakBLL.FetchByID(id.Value);
            if (fakultet == null)
            {
                return HttpNotFound();
            }

            var bllOrgJed = new OrganizacijskaJedinicaBLL();
            var OrgJeds = bllOrgJed.FetchAll().Where(x => x.FakultetId == fakultet.Id);

            FakultetViewModel model = new FakultetViewModel
            {
                Fakultet = fakultet,
                OrgJedList = OrgJeds.ToList(),
                PagingInfo = null
            };

            return View(model);
        }

        /// <summary>
        /// Upravitelj za dohvaćanje podataka potrebnih za stranicu za kreiranje fakulteta.
        /// </summary>
        /// <param name="sveucID">Šifra sveučilišta kojem pripada fakultet koji kreiramo</param>
        /// <returns>Pogled za kreiranje fakulteta</returns>
        public ActionResult Create(int? sveucID)
        {
            var sveucilisteBLL = new BLL.BLLProviders.SveucilisteBLL();
            ViewBag.SveucilisteId = new SelectList(sveucilisteBLL.FetchAll(), "Id", "Naziv", sveucID);
            return View();
        }

        /// <summary>
        /// Upravitelj za dohvaćanje podataka potrebnih za stranicu za kreiranje fakulteta.
        /// </summary>
        /// <param name="fakultet">Novi fakultet koji se dodaje</param>
        /// <returns>Pogled za kreiranje fakulteta</returns>
        [HttpPost][ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "SveucilisteId,Naziv,Kratica,Adresa,OIB,URL,IBAN")] BLL.Fakultet fakultet)
        {
            if (ModelState.IsValid)
            {
                fakBLL.Insert(fakultet);
                return RedirectToAction("Index");
            }
            var sveucilisteBLL = new BLL.BLLProviders.SveucilisteBLL();
            object selectedSv = sveBLL.FetchByID(fakultet.SveucilisteId);
            ViewBag.SveucilisteId = new SelectList(sveucilisteBLL.FetchAll(), "Id", "Naziv", selectedSv);
            return View(fakultet);
        }


        /// <summary>
        /// Upravitelj za dohvaćanje i prikaz podataka o fakultetu koji se uređuje.
        /// </summary>
        /// <param name="id">Broj fakulteta koji se uređuje</param>
        /// <returns>Pogled za uređivanje fakulteta</returns>
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var fakultet = fakBLL.FetchByID(id.Value);
            if (fakultet == null)
            {
                return HttpNotFound();
            }
            var sveucilisteBLL = new BLL.BLLProviders.SveucilisteBLL();
            ViewBag.SveucilisteId = new SelectList(sveucilisteBLL.FetchAll(), "Id", "Naziv", fakultet.SveucilisteId);
            return View(fakultet);
        }

        /// <summary>
        /// POST metoda za uređivanje fakulteta
        /// </summary>
        /// <param name="fakultet">Podaci o fakultetu koje je potrebno spremiti</param>
        /// <returns>Stranicu Index na fakultetu koji je bio uređen</returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,SveucilisteId,Naziv,Kratica,Adresa,OIB,URL,IBAN")] BLL.Fakultet fakultet)
        {
            if (ModelState.IsValid)
            {
                fakBLL.Update(fakultet);
                return RedirectToAction("Index");
            }
            var sveucilisteBLL = new BLL.BLLProviders.SveucilisteBLL();
            ViewBag.SveucilisteId = new SelectList(sveucilisteBLL.FetchAll(), "Id", "Naziv", fakultet.SveucilisteId);
            return View(fakultet);
        }


        /// <summary>
        /// Upravitelj za dohvaćanje podataka za prikaz pogleda za potvrđivanje brisanja.
        /// </summary>
        /// <param name="id">Broj računa koji se briše</param>
        /// <returns>Stranicu za potvrdu brisanja računa</returns>
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var fakultet = fakBLL.FetchByID(id.Value);
            if (fakultet == null)
            {
                return HttpNotFound();
            }
            return View(fakultet);
        }

        /// <summary>
        /// POST metoda koja se poziva kad se potvrdi brisanje fakulteta.
        /// </summary>
        /// <param name="id">Broj fakulteta koji se briše</param>
        /// <returns>Osvježenu Index stranicu s preostalim fakultetima</returns>
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var fakultet = fakBLL.FetchByID(id);
            fakBLL.Delete(fakultet);
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
