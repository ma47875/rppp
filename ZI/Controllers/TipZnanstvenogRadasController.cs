﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BLL;

namespace ZI.Controllers
{
    public class TipZnanstvenogRadasController : Controller
    {
        private BLL.BLLProviders.TipZnanstvenogRadaBLL tip_p = new BLL.BLLProviders.TipZnanstvenogRadaBLL();
        // GET: TipZnanstvenogRadas
        public ActionResult Index()
        {
            return View(tip_p.FetchAll());
        }

        // GET: TipZnanstvenogRadas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipZnanstvenogRada tipZnanstvenogRada = tip_p.Fetch(id.Value);
            if (tipZnanstvenogRada == null)
            {
                return HttpNotFound();
            }
            return View(tipZnanstvenogRada);
        }

        // GET: TipZnanstvenogRadas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipZnanstvenogRadas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Naziv")] TipZnanstvenogRada tipZnanstvenogRada)
        {
            if (ModelState.IsValid)
            {
                tip_p.Update(tipZnanstvenogRada);
                return RedirectToAction("Index");
            }

            return View(tipZnanstvenogRada);
        }

        // GET: TipZnanstvenogRadas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipZnanstvenogRada tipZnanstvenogRada = tip_p.Fetch(id.Value);
            if (tipZnanstvenogRada == null)
            {
                return HttpNotFound();
            }
            return View(tipZnanstvenogRada);
        }

        // POST: TipZnanstvenogRadas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Naziv")] TipZnanstvenogRada tipZnanstvenogRada)
        {
            if (ModelState.IsValid)
            {
                tip_p.Update(tipZnanstvenogRada);
                return RedirectToAction("Index");
            }
            return View(tipZnanstvenogRada);
        }

        // GET: TipZnanstvenogRadas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipZnanstvenogRada tipZnanstvenogRada = tip_p.Fetch(id.Value);
            if (tipZnanstvenogRada == null)
            {
                return HttpNotFound();
            }
            return View(tipZnanstvenogRada);
        }

        // POST: TipZnanstvenogRadas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            TipZnanstvenogRada tipZnanstvenogRada = tip_p.Fetch(id);
            tip_p.Delete(tipZnanstvenogRada);
            return RedirectToAction("Index");
        }

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
    }
}
