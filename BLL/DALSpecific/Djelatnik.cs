﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public partial class Djelatnik
    {
        internal void LoadFromDB(DAL.Djelatnik d, DAL.Zvanje z, DAL.Zaposlenje zap, DAL.OrganizacijskaJedinica f, DAL.TipDjelatnika t)
        {
            this.Id = d.Id;
            this.ZaposlenjeId = d.ZaposlenjeId;
            this.OrgJedId = d.OrgJedId;
            this.Ime = d.Ime;
            this.Prezime = d.Prezime;
            this.OIB = d.OIB;
            this.GodRodjenja = d.GodRodjenja;
            this.ZvanjeId = d.ZvanjeId;
            this.UredSoba = d.UredSoba.Trim();
            this.E_mail = d.E_mail.Trim();
            this.Telefon = d.Telefon.Trim();
            this.TipDjelatnikaId = d.TipDjelatnikaId;
            this.Zvanje = z.Naziv;
            this.Zaposlenje = zap.Naziv;
            this.OrgJed = f.Naziv;
            this.TipDjelatnika = t.Vrsta;
        }
    }
}
