﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public partial class Sveuciliste
    {
        internal void LoadFromDB(DAL.Sveuciliste s)
        {
            this.Id = s.Id;
            this.Naziv = s.Naziv;
            this.Adresa = s.Adresa;
            this.IBAN = s.IBAN;
            this.OIB = s.OIB;
            this.URL = s.URL;

        }
    }
}
