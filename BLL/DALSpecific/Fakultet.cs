﻿using System.Data;

namespace BLL
{

    public partial class Fakultet
    {
        internal void LoadFromDB(DAL.Fakultet fak)
        {
            this.Id = fak.Id;
            this.SveucilisteId = fak.SveucilisteId;
            this.SveucilisteNaziv = fak.Sveuciliste.Naziv;
            this.Naziv = fak.Naziv;
            this.Kratica = fak.Kratica;
            this.Adresa = fak.Adresa;
            this.OIB = fak.OIB;
            this.URL = fak.URL;
            this.IBAN = fak.IBAN;
        }
    }
}
