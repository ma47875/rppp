﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public partial class Predmet
    {
        internal void LoadFromDB(DAL.Predmet pred)
        {
            this.Id = pred.Id;
            this.TipPredmetaId = pred.TipPredmetaId;
            this.TipPredmeta = pred.TipPredmeta.Naziv;
            this.StudijskiProgram = pred.StudijskiProgram.Naziv;
            this.StudijskiProgramId = pred.StudijskiProgramId;
            this.Naziv = pred.Naziv;
            this.IshodiUcenja = pred.IshodiUcenja;
        }
    }
}
