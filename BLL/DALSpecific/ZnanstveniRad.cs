﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public partial class ZnanstveniRad
    {
        internal void LoadFromDB(DAL.ZnanstveniRad zr, DAL.TipZnanstvenogRada tip)
        {
            this.Id = zr.Id;
            this.NastavnoOsobljeId = zr.NastavnoOsobljeId;
            this.TipZnanstvenogRadaId = zr.TipZnanstvenogRadaId;
            this.Naziv = zr.Naziv;
            this.Godina = zr.Godina.Value;
            this.SifraUpisa = zr.SifraUpisa;
            this.TipZnanstvenogRada = tip.Naziv;
        }
    }
}
