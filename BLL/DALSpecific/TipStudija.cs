﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public partial class TipStudija
    {
        internal void LoadFromDB(DAL.TipStudija ts)
        {
            this.Id = ts.Id;
            this.Naziv = ts.Naziv;
        }
    }
}
