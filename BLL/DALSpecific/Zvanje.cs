﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public partial class Zvanje
    {
        internal void LoadFromDB(DAL.Zvanje z)
        {
            this.Id = z.Id;
            this.Naziv = z.Naziv;
        }
    }
}
