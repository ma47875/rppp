﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public partial class TipPredmeta
    {
        internal void LoadFromDB(DAL.TipPredmeta tp)
        {
            this.Id = tp.Id;
            this.Naziv = tp.Naziv;
        }
    }
}
