﻿namespace BLL
{
    public partial class OrganizacijskaJedinica
    {
        internal void LoadFromDB(DAL.OrganizacijskaJedinica oj)
        {
            this.Id = oj.Id;
            this.FakultetId = oj.FakultetId;
            this.TipOrgJedId = oj.TipOrgJedId;
            this.TipOrgJedNaziv = oj.TipOrgJed.Naziv;
            this.KarticaOrgJedId = oj.KarticaOrgJedId;
            this.Naziv = oj.Naziv;
            this.Kratica = oj.Kratica;
            this.Email = oj.Email;
        }
    }
}
