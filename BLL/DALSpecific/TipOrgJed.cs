﻿namespace BLL
{
    public partial class TipOrgJed
    {
        internal void LoadFromDB(DAL.TipOrgJed tip)
        {
            this.Id = tip.Id;
            this.Naziv = tip.Naziv;
        }
    }
}
