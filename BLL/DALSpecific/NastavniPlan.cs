﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public partial class NastavniPlan
    {
        internal void LoadFromDb(DAL.NastavniPlan np)
        {
            var fbll = new BLLProviders.FakultetBLL();

            this.Id = np.Id;
            this.PredmetId = np.PredmetId;
            this.ECTS = np.ECTS;
            this.Semestar = np.Semestar;
            this.GodinaOdrzavanja = np.GodinaOdržavanja;
            this.SatiPredavanja = np.SatiLaboratorija;
            this.SatiLaboratorija = np.SatiLaboratorija;
            this.SatiVjezbi = np.SatiVježbi;
        }
    }
}
