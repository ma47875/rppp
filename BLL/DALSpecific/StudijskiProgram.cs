﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public partial class StudijskiProgram
    {
        internal void LoadFromDb(DAL.StudijskiProgram sp)
        {
            var fbll = new BLLProviders.FakultetBLL();

            this.Id = sp.Id;
            this.Naziv = sp.Naziv;
            this.Trajanje = sp.Trajanje;
            this.FakultetId = sp.FakultetId;
            this.Fakultet = fbll.FetchByID(sp.FakultetId).Naziv;
            this.TipStudija = sp.TipStudija.Naziv;
        }
    }
}
