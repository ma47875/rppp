﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;

namespace BLL
{
    public partial class Projekt
    {
        internal void LoadFromDB(DAL.Projekt dalProjekt)
        {
            this.Id = dalProjekt.Id;
            this.TipProjektaId = dalProjekt.TipProjektaId;
            this.Naziv = dalProjekt.Naziv;
            this.KarticaProjektaId = dalProjekt.KarticaProjektaId;
        }
    }
}
