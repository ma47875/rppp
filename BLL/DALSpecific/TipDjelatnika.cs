﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public partial class TipDjelatnika
    {
        internal void LoadFromDB(DAL.TipDjelatnika t)
        {
            this.Id = t.Id;
            this.Vrsta = t.Vrsta;
        }
    }
}
