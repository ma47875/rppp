﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public partial class TipZnanstvenogRada
    {
        internal void LoadFromDB(DAL.TipZnanstvenogRada z)
        {
            this.Id = z.Id;
            this.Naziv = z.Naziv;
        }
    }
}
