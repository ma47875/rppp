﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public partial class TipZnanstvenogRada
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(50)]
        public string Naziv { get; set; }
    }
}
