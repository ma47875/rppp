﻿using System.ComponentModel.DataAnnotations;

namespace BLL
{
    /// <summary>
    /// Razred poslovnog sloja koji učahuruje podatke o studijskom programu
    /// </summary>
    public partial class StudijskiProgram
    {
        public int Id { get; set; }
        [Required]
        [MinLength(3)]
        public string Naziv { get; set; }
        [Required]
        [Range(0, 12)]
        public int Trajanje { get; set; }
        public string TipStudija { get; set; }
        public string Fakultet { get; set; }
        [Display(Name = "Tip studija")]
        public int TipStudijaId { get; set; }
        [Display(Name = "Fakultet")]
        public int FakultetId { get; set; }
    }
}
