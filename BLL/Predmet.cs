﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace BLL
{
    public partial class Predmet
    {
        public int Id { get; set; }
        public int TipPredmetaId { get; set; }
        public string TipPredmeta { get; set; }
        public int StudijskiProgramId { get; set; }
        public string StudijskiProgram { get; set; }
        [Required]
        public string Naziv { get; set; }
        [Required]
        public string IshodiUcenja { get; set; }

        private DAL.PredmetDAL dal = new DAL.PredmetDAL();
        public List<Predmet> FetchAll()
        {
            var list = new List<Predmet>();
            var dalCollection = dal.FetchAll();
            if (dalCollection is IEnumerable<DAL.Predmet>)
            {
                foreach (var dalObject in (IEnumerable<DAL.Predmet>)dalCollection)
                {
                    Predmet predmet = new Predmet();
                    predmet.LoadFromDB(dalObject);
                    //System.Console.WriteLine("Predmet: " + predmet.Naziv);
                    list.Add(predmet);
                   
                }
            }
            return list;
        }

        public BindingList<Predmet> FetchByStudijskiProgramId(int id)
        {
            BindingList<Predmet> list = new BindingList<Predmet>();
            var dalCollection = dal.FetchByStudijskiProgramId(id);
            if (dalCollection is IEnumerable<DAL.Predmet>)
            {
                foreach (var dalObject in (IEnumerable<DAL.Predmet>)dalCollection)
                {
                    Predmet predmet = new Predmet();
                    predmet.LoadFromDB(dalObject);
                    list.Add(predmet);

                }
            }
            return list;
        }
    }
}
