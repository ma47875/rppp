﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public partial class Projekt : IDataErrorInfo
    {
        public int Id { get; set; }
        public int TipProjektaId { get; set; }
        public string Naziv { get; set; }
        public int KarticaProjektaId { get; set; }
        public string Error
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                string s = this["Id"];
                if (!string.IsNullOrEmpty(s))
                {
                    sb.AppendLine(s);
                }
                s = this["Naziv"];
                if (!string.IsNullOrEmpty(s))
                {
                    sb.AppendLine(s);
                }
                s = this["TipProjektaId"];
                if (!string.IsNullOrEmpty(s))
                {
                    sb.AppendLine(s);
                }
                s = this["KarticaProjektaId"];
                if (!string.IsNullOrEmpty(s))
                {
                    sb.AppendLine(s);
                }
                return sb.ToString();
            }
        }
        public string this[string columnName]
        {

            get
            {
                string error = "";
                switch (columnName)
                {
                    case "Id":
                        if (Id == 0)
                            error = "Id projekta je obavezno polje!";
                        //TO DO: provjeriti postoji li artikl s ovom šifrom            
                        break;
                    case "Naziv":
                        if (string.IsNullOrEmpty(Naziv))
                        {
                            error = "Naziv projekta je obavezno polje!";
                        }

                        else if (Naziv.Length > 255)
                        {
                            error = string.Format("Maksimalna duljina naziva je {0}.", 255);
                        }
                        else
                        {
                            //TO DO naziv artikla mora biti jedinstven
                        }
                        break;
                    case "TipProjektaId":
                        if (TipProjektaId == 0)
                        {
                            error = "TipProjektaId je obavezno polje!";
                        }
                        break;
                    case "KarticaProjektaId":
                        if (KarticaProjektaId == 0)
                        {
                            error = "KarticaProjektaId je obavezno polje!";
                        }
                        break;
                    default: break;
                }
                return error;
            }
        }
    }
}
