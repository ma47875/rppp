﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public partial class ZnanstveniRad
    {
        public int Id { get; set; }
        [Required]
        public int NastavnoOsobljeId { get; set; }
        [Required]
        public int TipZnanstvenogRadaId { get; set; }
        public string TipZnanstvenogRada { get; set; }
        [Required]
        [MaxLength(50)]
        public string Naziv { get; set; }
        [Required]
        [Range(1900, 2016, ErrorMessage = "Neispravna godina rada")]
        public int Godina { get; set; }
        [Required]
        [MaxLength(10)]
        public string SifraUpisa { get; set; }
    }
}
