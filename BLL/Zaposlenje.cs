﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public partial class Zaposlenje
    {
        public int Id { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-ZčćžšđČĆŽŠĐ''-'\s]{1,30}$", ErrorMessage = "Zaposlenje ne sadrži znamenke")]
        public string Naziv { get; set; }
    }
}
