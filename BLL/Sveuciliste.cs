﻿using System.ComponentModel;

namespace BLL
{
    public partial class Sveuciliste
    {
        public int Id { get; set; }
        public string Naziv { get; set; }
        public string Kratica { get; set; }
        public string Adresa { get; set; }
        public string IBAN { get; set; }
        public string OIB { get; set; }
        public string URL { get; set; }

        public virtual BindingList<Fakultet> Fakulteti
        {
            get
            {
                var provider = new BLL.BLLProviders.FakultetBLL();
                return provider.FetchBySveuciliste(Id);
            }
        }

    }


}
