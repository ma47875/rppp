﻿using DAL;
using System.Collections.Generic;
using System.ComponentModel;

namespace BLL.BLLProviders
{
    /// <summary>
    /// Razred sa CRUD postupcima za rad sa studijskim programima (poslovni sloj)
    /// </summary>
    public partial class NastavniPlanBLL
    {
        private NastavniPlanDAL dal = new DAL.NastavniPlanDAL();

        /// <summary>
        /// Postupak za dohvat nastavnih planova s opcionalnim kriterijima filtriranja
        /// </summary>
        /// <param name="fakultetId">Kriterij za dohvat samo studijskih programa koji se provode na fakultetu s primarnim ključem zadanim ovim parametrom</param>
        /// <param name="tipStudijaId">Kriterij za dohvat samo studijskih programa koji su tipa zadani ovom šifrom tipa studija</param>
        /// <returns>Lista razreda studijskih programa u poslovnom sloju</returns>
        /// 

        public List<NastavniPlan> Fetch(int? predmetId = null, int? semestar = null, int? godina = null)
        {
            var list = new List<NastavniPlan>();
            var dalCollection = dal.Fetch(predmetId, semestar, godina);

            foreach (var dalObject in dalCollection)
            {
                var np = new NastavniPlan();
                np.LoadFromDb(dalObject);
                list.Add(np);
            }

            return list;
        }
        
        /// <summary>
        /// Postupak za dohvat studijskog programa po primarnom ključu
        /// </summary>
        /// <param name="id">Primarni ključ studijskog programa za dohvat</param>
        /// <returns>Razred poslovnog sloja koji učahuruje studijski program</returns>
        public NastavniPlan FetchById(int id)
        {
            NastavniPlan np = null;
            var dalObject = dal.FetchById(id);
            if (dalObject != null)
            {
                np = new NastavniPlan();
                np.LoadFromDb(dalObject);
            }
            return np;
        }

        /// <summary>
        /// Postupak za pohranu novog nastavnog plana
        /// </summary>
        /// <param name="sp">Razred poslovnog sloja koji učahuruje podatke o novom nastavnom planu</param>
        public void Insert(NastavniPlan np)
        {
            dal.Insert(np.PredmetId, np.Semestar, np.GodinaOdrzavanja, np.ECTS, np.SatiPredavanja, np.SatiLaboratorija, np.SatiVjezbi);
        }

        /// <summary>
        /// Postupak za izmjene podataka o nastavnom planu
        /// </summary>
        /// <param name="sp">Razred poslovnog sloja koji učahuruje podatke o izmijenjenom nastavnom planu</param>
        public void Update(NastavniPlan np)
        {
            dal.Update(np.Id, np.PredmetId, np.Semestar, np.GodinaOdrzavanja, np.ECTS, np.SatiPredavanja, np.SatiLaboratorija, np.SatiVjezbi);
        }

        /// <summary>
        /// Postupak za brisanje nastavnog plana
        /// </summary>
        /// <param name="id">Primarni ključ nastavnog plana koji se želi brisati</param>
        public void Delete(int id)
        {
            dal.Delete(id);
        }
    }
}
