﻿using DAL;
using System.Collections.Generic;
using System.ComponentModel;

namespace BLL.BLLProviders
{
    public class ZvanjeBLL
    {
        private ZvanjeDAL dal = new ZvanjeDAL();

        public BindingList<Zvanje> FetchAll()
        {
            BindingList<Zvanje> list = new BindingList<Zvanje>();
            var dalCollection = dal.FetchAll();

            foreach (var dalObject in (IEnumerable<DAL.Zvanje>)dalCollection)
            {
                Zvanje zva = new Zvanje();
                zva.LoadFromDB(dalObject);
                list.Add(zva);
            }

            return list;
        }

        public Zvanje Fetch(int id)
        {
            Zvanje zva = null;
            var dalObject = dal.Fetch(id);
            if (dalObject != null)
            {

                zva = new Zvanje();
                zva.LoadFromDB(dalObject);

            }
            return zva;

        }

        public void Insert(Zvanje z)
        {
            dal.Insert(z.Id, z.Naziv);
        }

        public void Delete(Zvanje z)
        {
            dal.Delete(z.Id);
        }
        public void Update(Zvanje z)
        {
            dal.Update(z.Id, z.Naziv);
        }
    }
}
