﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.BLLProviders
{
    public class TipZnanstvenogRadaBLL
    {
        private TipZnanstvenogRadaDAL dal = new TipZnanstvenogRadaDAL();

        public BindingList<TipZnanstvenogRada> FetchAll()
        {
            BindingList<TipZnanstvenogRada> list = new BindingList<TipZnanstvenogRada>();
            var dalCollection = dal.FetchAll();

            foreach (var dalObject in (IEnumerable<DAL.TipZnanstvenogRada>)dalCollection)
            {
                TipZnanstvenogRada tip = new TipZnanstvenogRada();
                tip.LoadFromDB(dalObject);
                list.Add(tip);
            }

            return list;
        }

        public TipZnanstvenogRada Fetch(int id)
        {
            TipZnanstvenogRada tip = null;
            var dalObject = dal.Fetch(id);
            if (dalObject != null)
            {

                tip = new TipZnanstvenogRada();
                tip.LoadFromDB(dalObject);

            }
            return tip;

        }

        public void Insert(TipZnanstvenogRada z)
        {
            dal.Insert(z.Id, z.Naziv);
        }

        public void Delete(TipZnanstvenogRada z)
        {
            dal.Delete(z.Id);
        }
        public void Update(TipZnanstvenogRada z)
        {
            dal.Update(z.Id, z.Naziv);
        }
    }
}
