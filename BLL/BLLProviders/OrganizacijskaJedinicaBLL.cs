﻿using DAL;
using System.ComponentModel;
using System.Data;
using System.Linq;

namespace BLL.BLLProviders
{
    /// <summary>
    /// Razred koji pruža funkcionalnosti BLL sloja za Org.Jedinice.
    /// </summary>
    public class OrganizacijskaJedinicaBLL
    {
        /// <summary>
        /// Pružatelj funkcionalnosti DAL sloja za Org.Jedinice.
        /// </summary>
        private OrganizacijskaJedinicaDAL dal = new OrganizacijskaJedinicaDAL();

        /// <summary>
        /// Postupak za dohvat svih dostupnih org.jedinica.
        /// </summary>
        /// <returns>Lista svih org.jedinica.</returns>
        public BindingList<OrganizacijskaJedinica> FetchAll()
        {
            var list = new BindingList<OrganizacijskaJedinica>();
            var dalCollection = dal.FetchOrgJedAll();

            foreach (var dalObject in dalCollection)
            {
                var sp = new OrganizacijskaJedinica();
                sp.LoadFromDB(dalObject);
                list.Add(sp);
            }

            return list;
        }

        /// <summary>
        /// Postupak za dohvaćanje određene org.jedinice
        /// </summary>
        /// <param name="Id">Šifra org.jedinice</param>
        /// <returns>Org. jedinica s traženom šifrom</returns>
        public OrganizacijskaJedinica FetchByID(int Id)
        {
            OrganizacijskaJedinica orgJed= null;
            var dalObject = dal.FetchOrgJedByID(Id);
            if (dalObject != null)
            {
                orgJed = new OrganizacijskaJedinica();
                orgJed.LoadFromDB(dalObject);
            }
            return orgJed;
        }

        public BindingList<OrganizacijskaJedinica> FetchAllCondition(int Id)
        {
            BindingList<OrganizacijskaJedinica> list = new BindingList<OrganizacijskaJedinica>();
            var dalCollection = dal.FetchAllCondition(Id);
            foreach (var dalObject in dalCollection)
            {
                OrganizacijskaJedinica oj = new OrganizacijskaJedinica();
                oj.LoadFromDB(dalObject);
                list.Add(oj);
            }
            return list;
        }


        /// <summary>
        /// Postupak za dohvaćanje org.jedinica koje pripadaju traženom fakultetu
        /// </summary>
        /// <param name="fakultetId">Šifra fakulteta</param>
        /// <returns>Lista org.jedinica koje pripadaju odabranom fakultetu</returns>
        public BindingList<OrganizacijskaJedinica> ForOrgJed(int fakultetId)
        {
            return new BindingList<OrganizacijskaJedinica>((from c in FetchAll()
                                                            where c.FakultetId == fakultetId
                                                            select c).ToList());
        }

        /// <summary>
        /// Postupak za spremanje org.jedinice u bazu
        /// </summary>
        /// <param name="oj">Podatci org.jed koje je potrebno spremiti</param>
        public void Insert(OrganizacijskaJedinica oj)
        {
            dal.Insert(oj.FakultetId, oj.TipOrgJedId, oj.KarticaOrgJedId, oj.Naziv, oj.Kratica, oj.Email);
        }

        /// <summary>
        /// Postupak za brisanje org.jedinice iz baze
        /// </summary>
        /// <param name="oj">Podatci org.jed koju je potrebno izbrisati</param>
        public void Delete(OrganizacijskaJedinica oj)
        {
            dal.Delete(oj.Id);
        }

        /// <summary>
        /// Postupak za ažuriranje podataka o org.jedinici u bazi
        /// </summary>
        /// <param name="oj">Podatci org.jedinice koje je potrebno spremiti</param>
        public void Update(OrganizacijskaJedinica oj)
        {
            dal.Update(oj.Id, oj.FakultetId, oj.TipOrgJedId, oj.KarticaOrgJedId, oj.Naziv, oj.Kratica, oj.Email);
        }

        public void RefreshOrgJed(object datasource, int position, object current)
        {
            BindingList<OrganizacijskaJedinica> list = (BindingList<OrganizacijskaJedinica>)datasource;
            OrganizacijskaJedinica oj = list[position];
            list[position] = FetchByID(oj.Id);
        }
    }
}
