﻿using DAL;
using System.Collections.Generic;
using System.ComponentModel;

namespace BLL.BLLProviders
{
    public class TipPredmetaBLL
    {
        private TipPredmetaDAL dal = new TipPredmetaDAL();

        public BindingList<TipPredmeta> FetchAll()
        {
            var list = new BindingList<TipPredmeta>();
            var dalCollection = dal.FetchAll();

            foreach (var dalObject in (IEnumerable<DAL.TipPredmeta>)dalCollection)
            {
                var tip = new TipPredmeta();
                tip.LoadFromDB(dalObject);
                list.Add(tip);
            }

            return list;
        }

        public TipPredmeta Fetch(int id)
        {
            TipPredmeta tip = null;
            var dalObject = dal.FetchById(id);
            if (dalObject != null)
            {

                tip = new TipPredmeta();
                tip.LoadFromDB(dalObject);

            }
            return tip;

        }
        public void Insert(TipPredmeta ts)
        {
            dal.Insert(ts.Naziv);
        }

        public void Update(TipPredmeta ts)
        {
            dal.Update(ts.Id, ts.Naziv);
        }

        public void Delete(int id)
        {
            dal.Delete(id);
        }
    }
}
