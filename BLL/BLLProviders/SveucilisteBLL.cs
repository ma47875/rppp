﻿using DAL;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

namespace BLL.BLLProviders
{
    public partial class SveucilisteBLL
    {
        private SveucilisteDAL dal = new SveucilisteDAL();

        public BindingList<Sveuciliste> FetchAll()
        {
            var list = new BindingList<Sveuciliste>();
            var dalCollection = dal.FetchAll();
            if (dalCollection is IEnumerable<DAL.Sveuciliste>)
            {
                foreach (var dalObject in (IEnumerable<DAL.Sveuciliste>)dalCollection)
                {
                    Sveuciliste s = new Sveuciliste();
                    s.LoadFromDB(dalObject);
                    list.Add(s);
                }
            }
            return list;
        }

        public Sveuciliste FetchByID(int Id)
        {
            Sveuciliste sv = null;
            var dalObject = dal.FetchSveucilisteByID(Id);
            if (dalObject != null)
            {
                sv = new Sveuciliste();
                sv.LoadFromDB(dalObject);
            }
            return sv;
        }
    }
}
