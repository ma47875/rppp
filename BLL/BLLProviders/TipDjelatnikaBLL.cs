﻿using DAL;
using System.Collections.Generic;
using System.ComponentModel;

namespace BLL.BLLProviders
{
    public class TipDjelatnikaBLL
    {
        private TipDjelatnikaDAL dal = new TipDjelatnikaDAL();

        public BindingList<TipDjelatnika> FetchAll()
        {
            BindingList<TipDjelatnika> list = new BindingList<TipDjelatnika>();
            var dalCollection = dal.FetchAll();

            foreach (var dalObject in (IEnumerable<DAL.TipDjelatnika>)dalCollection)
            {
                TipDjelatnika tip = new TipDjelatnika();
                tip.LoadFromDB(dalObject);
                list.Add(tip);
            }

            return list;
        }

        public TipDjelatnika Fetch(int id)
        {
            TipDjelatnika tip = null;
            var dalObject = dal.Fetch(id);
            if (dalObject != null)
            {

                tip = new TipDjelatnika();
                tip.LoadFromDB(dalObject);

            }
            return tip;

        }
    }
}
