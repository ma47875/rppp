﻿using DAL;
using System.Collections.Generic;
using System.ComponentModel;

namespace BLL.BLLProviders
{
    public partial class PredmetBLL
    {
        private PredmetDAL dal = new PredmetDAL();

        public List<Predmet> FetchAll(int? studijskiProgramId = null)
        {
            var list = new List<Predmet>();
            var dalCollection = dal.FetchAll(studijskiProgramId);

            foreach (var dalObject in dalCollection)
            {
                var p = new Predmet();
                p.LoadFromDB(dalObject);
                list.Add(p);
            }

            return list;
        }
        
        public Predmet FetchById(int id)
        {
            Predmet p = null;
            var dalObject = dal.FetchById(id);
            if (dalObject != null)
            {
                p = new Predmet();
                p.LoadFromDB(dalObject);
            }
            return p;
        }

        public void Insert(Predmet p)
        {
            dal.Insert(p.TipPredmetaId, p.StudijskiProgramId, p.Naziv, p.IshodiUcenja);
        }

        public void Update(Predmet p)
        {
            dal.Update(p.Id, p.TipPredmetaId, p.StudijskiProgramId, p.Naziv, p.IshodiUcenja);
        }

        public void Delete(int id)
        {
            dal.Delete(id);
        }
    }
}
