﻿using DAL;
using System.Collections.Generic;
using System.ComponentModel;

namespace BLL.BLLProviders
{
    public class DjelatnikBLL
    {
        private DjelatnikDAL dal = new DjelatnikDAL();

        public List<Djelatnik> FetchAll()
        {
            List<Djelatnik> list = new List<Djelatnik>();
            var dalCollection = dal.FetchAll();
            
                foreach (var dalObject in (IEnumerable<DAL.Djelatnik>)dalCollection)
                {
                    Djelatnik djelatnik = new Djelatnik();
                    djelatnik.LoadFromDB(dalObject, dalObject.Zvanje, dalObject.Zaposlenje, dalObject.OrganizacijskaJedinica, dalObject.TipDjelatnika);
                    list.Add(djelatnik);
                }
            
            return list;
        }

        public List<Djelatnik> FetchByOrgJed(int orgJedId)
        {
            List<Djelatnik> list = new List<Djelatnik>();
            var dalCollection = dal.FetchByOrgJed(orgJedId);

            foreach (var dalObject in (IEnumerable<DAL.Djelatnik>)dalCollection)
            {
                Djelatnik djelatnik = new Djelatnik();
                djelatnik.LoadFromDB(dalObject, dalObject.Zvanje, dalObject.Zaposlenje, dalObject.OrganizacijskaJedinica, dalObject.TipDjelatnika);
                list.Add(djelatnik);
            }

            return list;
        }

        public Djelatnik Fetch(int id)
        {
            Djelatnik djelatnik = null;
            var dalObject = dal.Fetch(id);
            if (dalObject != null)
            {
                
                    djelatnik = new Djelatnik();
                    djelatnik.LoadFromDB(dalObject, dalObject.Zvanje, dalObject.Zaposlenje, dalObject.OrganizacijskaJedinica, dalObject.TipDjelatnika);

            }
            return djelatnik;

        }

        public void Insert(BLL.Djelatnik d)
        {
                dal.Insert(d.Id, d.ZaposlenjeId, d.OrgJedId, d.Ime, d.Prezime, d.OIB, d.GodRodjenja,d.ZvanjeId, d.UredSoba, d.E_mail, d.Telefon, d.TipDjelatnikaId);
        }

        public void Delete(BLL.Djelatnik d)
        {
            dal.Delete(d.Id);
        }

        public void Update(Djelatnik d)
        {
                dal.Update(d.Id, d.ZaposlenjeId, d.OrgJedId, d.Ime, d.Prezime, d.OIB, d.GodRodjenja, d.ZvanjeId, d.UredSoba, d.E_mail, d.Telefon, d.TipDjelatnikaId);
        }
    }
}
