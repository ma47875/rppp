﻿using DAL;
using System.Collections.Generic;
using System.ComponentModel;

namespace BLL.BLLProviders
{
    /// <summary>
    /// Razred sa CRUD postupcima za rad sa studijskim programima (poslovni sloj)
    /// </summary>
    public partial class StudijskiProgramBLL
    {
        private StudijskiProgramDAL dal = new StudijskiProgramDAL();

        /// <summary>
        /// Postupak za dohvat studijskih programa s opcionalnim kriterijima
        /// </summary>
        /// <param name="fakultetId">Kriterij za dohvat samo studijskih programa koji se provode na fakultetu s primarnim ključem zadanim ovim parametrom</param>
        /// <param name="tipStudijaId">Kriterij za dohvat samo studijskih programa koji su tipa zadani ovom šifrom tipa studija</param>
        /// <returns>Lista razreda studijskih programa u poslovnom sloju</returns>
        public List<StudijskiProgram> FetchAll(int? fakultetId = null, int? tipStudijaId = null)
        {
            var list = new List<StudijskiProgram>();
            var dalCollection = dal.FetchAll(fakultetId, tipStudijaId);

            foreach (var dalObject in dalCollection)
            {
                var sp = new StudijskiProgram();
                sp.LoadFromDb(dalObject);
                list.Add(sp);
            }

            return list;
        }
        
        /// <summary>
        /// Postupak za dohvat studijskog programa po primarnom ključu
        /// </summary>
        /// <param name="id">Primarni ključ studijskog programa za dohvat</param>
        /// <returns>Razred poslovnog sloja koji učahuruje studijski program</returns>
        public StudijskiProgram FetchById(int id)
        {
            StudijskiProgram sp = null;
            var dalObject = dal.FetchById(id);
            if (dalObject != null)
            {
                sp = new StudijskiProgram();
                sp.LoadFromDb(dalObject);
            }
            return sp;
        }

        /// <summary>
        /// Postupak za dohvat studijskih programa koji se provode na zadanom fakultetu
        /// </summary>
        /// <param name="id">Primarni ključ fakulteta na kojem se traže studijski programi</param>
        /// <returns>Lista razreda studijskih programa u poslovnom sloju</returns>
        public List<StudijskiProgram> FetchByFakultetId(int id)
        {
            var list = new List<StudijskiProgram>();
            var dalCollection = dal.FetchByFakultetId(id);

            foreach (var dalObject in dalCollection)
            {
                var sp = new StudijskiProgram();
                sp.LoadFromDb(dalObject);
                list.Add(sp);
            }

            return list;
        }

        /// <summary>
        /// Postupak za dohvat predmeta na zadanom studijskom programu
        /// </summary>
        /// <param name="id">Primarni ključ studijskog programa za kojeg se traži dohvat predmeta</param>
        /// <returns>Lista razreda predmeta iz poslovnog sloja</returns>
        public List<Predmet> FetchPredmetiById(int id)
        {
            var list = new List<Predmet>();
            var dalCollection = dal.FetchPredmetiById(id);

            foreach (var dalObject in dalCollection)
            {
                var predmet = new Predmet();
                predmet.LoadFromDB(dalObject);
                list.Add(predmet);
            }

            return list;
        }

        /// <summary>
        /// Postupak za pohranu novog studijskog programa
        /// </summary>
        /// <param name="sp">Razred poslovnog sloja koji učahuruje podatke o novom studijskom programu</param>
        public void Insert(StudijskiProgram sp)
        {
            dal.Insert(sp.Naziv, sp.Trajanje, sp.TipStudijaId, sp.FakultetId);
        }

        /// <summary>
        /// Postupak za izmjene podataka o studijskom programu
        /// </summary>
        /// <param name="sp">Razred poslovnog sloja koji učahuruje podatke o izmijenjenom studijskom programu</param>
        public void Update(StudijskiProgram sp)
        {
            dal.Update(sp.Id, sp.Naziv, sp.Trajanje, sp.TipStudijaId, sp.FakultetId);
        }

        /// <summary>
        /// Postupak za brisanje studijskog programa
        /// </summary>
        /// <param name="id">Primarni ključ studijskog programa koji se želi brisati</param>
        public void Delete(int id)
        {
            dal.Delete(id);
        }
    }
}
