﻿using DAL;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

namespace BLL.BLLProviders
{
    /// <summary>
    /// Razred koji pruža funkcionalnosti BLL sloja za Fakultete.
    /// </summary>
    public partial class FakultetBLL
    {
        /// <summary>
        /// Pružatelj funkcionalnosti DAL sloja za Fakultete
        /// </summary>
        private FakultetDAL dal = new FakultetDAL();

        /// <summary>
        /// Postupak za dohvat svih dostupnih fakulteta
        /// </summary>
        /// <returns>Lista svih fakulteta</returns>
        public BindingList<Fakultet> FetchAll()
        {
            BindingList<Fakultet> list = new BindingList<Fakultet>();
            var dalCollection = dal.FetchFakultetAll();
            if (dalCollection is IEnumerable<DAL.Fakultet>)
            {
                foreach (var dalObject in (IEnumerable<DAL.Fakultet>)dalCollection)
                {
                    Fakultet fak = new Fakultet();
                    fak.LoadFromDB(dalObject);
                    list.Add(fak);
                }
            }
            return list;
        }

        /// <summary>
        /// Postupak za dohvaćanje određenog fakulteta
        /// </summary>
        /// <param name="Id">Šifra fakulteta</param>
        /// <returns>Fakultet sa traženom šifrom</returns>
        public Fakultet FetchByID(int Id)
        {
            Fakultet fak = null;
            var dalObject = dal.FetchFakultetByID(Id);
            if (dalObject != null)
            {
                fak = new Fakultet();
                fak.LoadFromDB(dalObject);
            }
            return fak;
        }

        /// <summary>
        /// Postupak za dohvaćanje fakulteta koji pripadaju traženom sveučilištu
        /// </summary>
        /// <param name="sveucilisteID">Šifra sveučilišta</param>
        /// <returns>Lista fakulteta koji pripadaju odabranom sveučilištu</returns>
        public BindingList<Fakultet> FetchBySveuciliste(int sveucilisteID)
        {
            BindingList<Fakultet> list = new BindingList<Fakultet>();
            var dalCollection = dal.FetchFakultetsBySveuciliste(sveucilisteID);
            if (dalCollection is IEnumerable<DAL.Fakultet>)
            {
                foreach (var dalObject in (IEnumerable<DAL.Fakultet>)dalCollection)
                {
                    Fakultet fak = new Fakultet();
                    fak.LoadFromDB(dalObject);
                    list.Add(fak);
                }
            }
            return list;
        }


        /// <summary>
        /// Postupak za spremanje fakulteta u bazu
        /// </summary>
        /// <param name="f">Podatci fakulteta koje je potrebno spremiti</param>
        public void Insert(Fakultet f)
        {
            dal.Insert(f.SveucilisteId, f.Naziv, f.Kratica, f.Adresa, f.OIB, f.URL, f.IBAN);
        }

        /// <summary>
        /// Postupak za brisanje fakulteta iz baze
        /// </summary>
        /// <param name="f">Podatci fakulteta kojeg je potrebno izbrisati</param>
        public void Delete(Fakultet f)
        {
            dal.Delete(f.Id);
        }

        /// <summary>
        /// Postupak za ažuriranje podataka o fakultetu u bazi
        /// </summary>
        /// <param name="f">Podatci fakulteta koje je potrebno spremiti</param>
        public void Update(Fakultet f)
        {
            dal.Update(f.Id, f.SveucilisteId, f.Naziv, f.Kratica, f.Adresa, f.OIB, f.URL, f.IBAN);
        }

        public void RefreshFakultet(object datasource, int position, object current)
        {
            BindingList<Fakultet> list = (BindingList<Fakultet>)datasource;
            Fakultet f = list[position];
            list[position] = FetchByID(f.Id);
        }

    }
}
