﻿using DAL;
using System.Collections.Generic;
using System.ComponentModel;

namespace BLL.BLLProviders
{
    public class ZaposlenjeBLL
    {
        private ZaposlenjeDAL dal = new ZaposlenjeDAL();

        public BindingList<Zaposlenje> FetchAll()
        {
            BindingList<Zaposlenje> list = new BindingList<Zaposlenje>();
            var dalCollection = dal.FetchAll();

            foreach (var dalObject in (IEnumerable<DAL.Zaposlenje>)dalCollection)
            {
                Zaposlenje zap = new Zaposlenje();
                zap.LoadFromDB(dalObject);
                list.Add(zap);
            }

            return list;
        }

        public Zaposlenje Fetch(int id)
        {
            Zaposlenje zap = null;
            var dalObject = dal.Fetch(id);
            if (dalObject != null)
            {

                zap = new Zaposlenje();
                zap.LoadFromDB(dalObject);

            }
            return zap;

        }

        public void Insert(Zaposlenje z)
        {
            dal.Insert(z.Id, z.Naziv);
        }

        public void Delete(Zaposlenje z)
        {
            dal.Delete(z.Id);
        }
        public void Update(Zaposlenje z)
        {
            dal.Update(z.Id, z.Naziv);
        }
    }
}
