﻿using System.Collections.Generic;
using System.ComponentModel;


namespace BLL.BLLProviders
{
    public class TipOrgJedBLL
    {
        private DAL.TipOrgJedDAL dal = new DAL.TipOrgJedDAL();

        public BindingList<TipOrgJed> FetchAll()
        {
            BindingList<TipOrgJed> list = new BindingList<TipOrgJed>();
            var dalCollection = dal.FetchAll();
            if (dalCollection is IEnumerable<DAL.TipOrgJed>)
            {
                foreach (var dalObject in (IEnumerable<DAL.TipOrgJed>)dalCollection)
                {
                    TipOrgJed orgJed = new TipOrgJed();
                    orgJed.LoadFromDB(dalObject);
                    list.Add(orgJed);
                }
            }
            return list;
        }

        public TipOrgJed FetchByID(int Id)
        {
            TipOrgJed tipOj = new TipOrgJed();
            var dalObject = dal.FetchTipOjByID(Id);
            if (dalObject != null)
            {
                tipOj.LoadFromDB(dalObject);
                return tipOj;
            }
            return null;
        }

        public void Insert(TipOrgJed toj)
        {
            dal.Insert(toj.Naziv);
        }

        public void Delete(TipOrgJed toj)
        {
            dal.Delete(toj.Id);
        }

        public void Update(TipOrgJed toj)
        {
            dal.Update(toj.Id, toj.Naziv);
        }

        public void RefreshTipOrgJed(object datasource, int position, object current)
        {
            BindingList<TipOrgJed> list = (BindingList<TipOrgJed>)datasource;
            TipOrgJed toj = list[position];
            list[position] = FetchByID(toj.Id);
        }
    }
}
