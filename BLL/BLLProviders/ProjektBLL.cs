﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace BLL.BLLProviders
{
    public class ProjektBLL
    {
        private ProjektDAL dal = new ProjektDAL();
        public BindingList<Projekt> FetchAll()
        {
            BindingList<Projekt> list = new BindingList<Projekt>();
            var dalCollection = dal.FetchAll();
            if (dalCollection is IEnumerable<DAL.Projekt>)
            {
                foreach (var dalObject in dalCollection as IEnumerable<DAL.Projekt>)
                {
                    Projekt projekt = new Projekt();
                    projekt.LoadFromDB(dalObject);
                    list.Add(projekt);
                }
            }
            return list;
        }
        public Projekt Fetch(int Id)
        {
            Projekt projekt = null;
            var dalObject = dal.Fetch(Id);
            if ( dalObject != null )
            {
                projekt = new Projekt();
                projekt.LoadFromDB(dalObject);
            }
            return projekt;
        }
        public void Insert(Projekt p)
        {
            if (string.IsNullOrEmpty(p.Error))
            {
                dal.Insert(p.Id, p.Naziv, p.TipProjektaId, p.KarticaProjektaId);
            }
            else
            {
                throw new Exception("Validacija neuspješna: " + p.Error);
            }
        }
        public void Update(Projekt p)
        {
            if (string.IsNullOrEmpty(p.Error))
            {
                dal.Update(p.Id, p.Naziv, p.TipProjektaId, p.KarticaProjektaId);
            }
            else
            {
                throw new Exception("Validacija neuspješna: " + p.Error);
            }
        }
        public void Delete(Projekt p)
        {
            dal.Delete(p.Id);
        }
    }
}
