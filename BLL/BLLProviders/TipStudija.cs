﻿using DAL;
using System.Collections.Generic;
using System.ComponentModel;

namespace BLL.BLLProviders
{
    public class TipStudijaBLL
    {
        private DAL.TipStudijaDAL dal = new DAL.TipStudijaDAL();

        public BindingList<TipStudija> FetchAll()
        {
            var list = new BindingList<TipStudija>();
            var dalCollection = dal.FetchAll();

            foreach (var dalObject in (IEnumerable<DAL.TipStudija>)dalCollection)
            {
                var tip = new TipStudija();
                tip.LoadFromDB(dalObject);
                list.Add(tip);
            }

            return list;
        }

        public TipStudija Fetch(int id)
        {
            TipStudija tip = null;
            var dalObject = dal.FetchById(id);
            if (dalObject != null)
            {

                tip = new TipStudija();
                tip.LoadFromDB(dalObject);

            }
            return tip;

        }

        public void Insert(TipStudija ts)
        {
            dal.Insert(ts.Naziv);
        }

        public void Update(TipStudija ts)
        {
            dal.Update(ts.Id, ts.Naziv);
        }

        public void Delete(int id)
        {
            dal.Delete(id);
        }
    }
}
