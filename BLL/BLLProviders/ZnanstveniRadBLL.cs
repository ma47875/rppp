﻿using DAL;
using System.Collections.Generic;
using System.ComponentModel;

namespace BLL.BLLProviders
{
    public class ZnanstveniRadBLL
    {
        private ZnanstveniRadDAL dal = new ZnanstveniRadDAL();

        public BindingList<ZnanstveniRad> FetchAll()
        {
            BindingList<ZnanstveniRad> list = new BindingList<ZnanstveniRad>();
            var dalCollection = dal.FetchAll();

            foreach (var dalObject in (IEnumerable<DAL.ZnanstveniRad>)dalCollection)
            {
                ZnanstveniRad rad = new ZnanstveniRad();
                rad.LoadFromDB(dalObject, dalObject.TipZnanstvenogRada);
                list.Add(rad);
            }

            return list;
        }

        public ZnanstveniRad Fetch(int id)
        {
            ZnanstveniRad rad = null;
            var dalObject = dal.Fetch(id);
            if (dalObject != null)
            {

                rad = new ZnanstveniRad();
                rad.LoadFromDB(dalObject, dalObject.TipZnanstvenogRada);

            }
            return rad;

        }

        public void Insert(BLL.ZnanstveniRad z)
        {
            dal.Insert(z.Id, z.NastavnoOsobljeId, z.TipZnanstvenogRadaId, z.Naziv, z.Godina, z.SifraUpisa);
        }

        public void Delete(BLL.ZnanstveniRad z)
        {
            dal.Delete(z.Id);
        }
        public void Update(BLL.ZnanstveniRad z)
        {
            dal.Update(z.Id, z.NastavnoOsobljeId, z.TipZnanstvenogRadaId, z.Naziv, z.Godina, z.SifraUpisa);
        }
    }
}
