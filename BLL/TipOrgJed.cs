﻿using System.ComponentModel.DataAnnotations;

namespace BLL
{
    public partial class TipOrgJed
    {
        public int Id { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-ZčćžšđČĆŽŠĐ''-'\s]{1,30}$", ErrorMessage = "Naziv ne sadrži znamenke!")]
        public string Naziv { get; set; }

    }
}
