﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL
{
    public partial class Djelatnik
    {
        public int Id { get; set; }
        [Required]
        public int ZaposlenjeId { get; set; }
        public string Zaposlenje { get; set; }
        [Required]
        public int OrgJedId { get; set; }
        public string OrgJed { get; set; }
        [Required]
        [MaxLength(30)]
        [RegularExpression(@"^[a-zA-ZčćžšđČĆŽŠĐ''-'\s]{1,30}$", ErrorMessage = "Ime ne sadrži znamenke")]
        public string Ime { get; set; }
        [Required]
        [MaxLength(50)]
        [RegularExpression(@"^[a-zA-ZčćžšđČĆŽŠĐ''-'\s]{1,50}$", ErrorMessage = "Prezime ne sadrži znamenke")]
        public string Prezime { get; set; }
        [Required]
        [MaxLength(13)]
        [RegularExpression("^[0-9]{13}$", ErrorMessage = "OIB mora imati 13 znamenki")]
        public string OIB { get; set; }
        [Required]
        [Range (1930, 1995, ErrorMessage = "Djelatnik se mora roditi izmeđju 1930. i 1995. godine")]
        public int GodRodjenja { get; set; }
        [Required]
        public int ZvanjeId { get; set; }
        public string Zvanje { get; set; }
        [Required]
        [MaxLength(15)]
        public string UredSoba { get; set; }
        [Required]
        [MaxLength(25)]
        public string E_mail { get; set; }
        [Required]
        [MaxLength(15)]
        public string Telefon { get; set; }
        [Required]
        public int TipDjelatnikaId { get; set; }
        public string TipDjelatnika { get; set; }

    }
}
