﻿using System.ComponentModel.DataAnnotations;

namespace BLL
{
    /// <summary>
    /// Razred poslovnog sloja koji učahuruje podatke o studijskom programu
    /// </summary>
    public partial class NastavniPlan
    {
        public int Id { get; set; }
        [Required]
        public int PredmetId { get; set; }
        [Required]
        public int GodinaOdrzavanja { get; set; }
        [Required]
        [Range(1, 12)]
        public int Semestar { get; set; }
        [Required]
        [Range(0, 180)]
        public int ECTS { get; set; }
        [Required]
        public int SatiPredavanja { get; set; }
        [Required]
        public int SatiLaboratorija { get; set; }
        [Required]
        public int SatiVjezbi { get; set; }
    }
}
