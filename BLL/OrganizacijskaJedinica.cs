﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BLL
{
    public partial class OrganizacijskaJedinica
    {
        public int Id { get; set; }

        public int FakultetId { get; set; }

        public int TipOrgJedId { get; set; }

        public string TipOrgJedNaziv { get; set; }

        public Nullable<int> KarticaOrgJedId { get; set; }

        [Required]
        [RegularExpression(@"^[a-zA-ZčćžšđČĆŽŠĐ''-'\s]{1,30}$", ErrorMessage = "Naziv ne sadrži znamenke")]
        public string Naziv { get; set; }

        [Required][MaxLength(10)]
        public string Kratica { get; set; }

        [Required][EmailAddress]
        public string Email { get; set; }

    }
}
       