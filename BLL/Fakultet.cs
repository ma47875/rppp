﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;


namespace BLL
{
    public partial class Fakultet 
    {
      
        public int Id { get; set; }
        public int SveucilisteId { get; set; }
        [Required]
        [RegularExpression(@"^[a-zA-ZčćžšđČĆŽŠĐ''-'\s]{1,30}$", ErrorMessage = "Naziv ne sadrži znamenke")]
        public string Naziv { get; set; }
        [Required]
        [MaxLength(5)]
        public string Kratica { get; set; }
        [Required]
        public string Adresa { get; set; }
        [Required]
        [MaxLength(11)]
        [RegularExpression("^[0-9]+$", ErrorMessage = "OIB ne sadrži slova!")]
        public string OIB { get; set; }
        [Required]
        [Url]
        public string URL { get; set; }
        [Required]
        public string IBAN { get; set; }
        public string SveucilisteNaziv { get; set; }

        public virtual BindingList<OrganizacijskaJedinica> OrganizacijskeJedinice
        {
            get
            {
                var provider = new BLL.BLLProviders.OrganizacijskaJedinicaBLL();
                return provider.ForOrgJed(Id);
            }
        }

    }


}
